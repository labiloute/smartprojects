<?php
// Internal classes
include_once('templates/task.php');
include_once('templates/project.php');
include_once('include/adressbook.php');

$Task=new Task($_CONFIG);
$Project=new Project($_CONFIG,$Task);
$ABook=new AdressBook($_CONFIG);

// ***********************/
// Session settings ******/
// ***********************/

include_once('include/vars.php');

// ***********************/
// Read adress book ******/
// ***********************/

$ABook->GetFromFile();

// ***********************/
// Table header ******/
// ***********************/

//var_dump($_SESSION);

echo '
	<div id="projects_wrapper">
	<table id="projects-table">';
	echo '<thead>';
	echo '<th colspan="2">Projet</th>';
	//echo "<th>Objectif</th>";
	echo "<th>Ordre</th>";
	echo "<th>Échéance</th>";
	echo '<th>PM <span class="circled" title="Project Manager (projet ou tâche)&#13;Séparateur : virgule.">?<span></th>';
	echo '<th>Équipe projet <span class="circled" title="Séparateur : virgule.">?<span></th>';
	echo '<th>Coût <span class="circled" title="'.$_CONFIG['currency'].'">?<span></th>';
	echo "<th>% temporel</th>";
	echo "<th>Étape suivante</th>";
	echo "<th>Actions ".$archive_link." ".$showdone_link."</th>";
	echo '</thead>';
	echo '<tbody>';

// *******************/
// Projects Loop *****/
// *******************/
$pcount=1;
$pabook=array();
foreach(glob($basepath.'/'.'[0-9][0-9]*', GLOB_ONLYDIR) as $project) {
	// Display project (and tasks)
	$rproject=$Project->displayproject($pcount,$project);
	// Store adress book
	$pabook=array_merge($pabook,$Project->pabook);
	// ?
	if (! $rproject) continue;
	$pcount++;
}

// ***************************/
// Adress book rewrite  ******/
// ***************************/

$ABook->Update($pabook);


// *******************/
// New project *******/
// *******************/

	$htmlpriority='<select name="newproject_priority" id="newproject_priority" class="new_priority">';
	for ($z=1;$z<100;$z++) {
		($z==50)?$sel='selected="selected"':$sel='';
		$htmlpriority.='<option value="'.$z.'" '.$sel.' >'.$z.'</option>';
		}
	$htmlpriority.='</select>';

	echo '<td colspan="2"><input id="newproject_name" placeholder="Nom_du_projet" pattern="[a-zA-Z0-9_-]+"/></td>';
	echo '<td>'.$htmlpriority.'</td>';
	echo '<td colspan="7"><input type="button" id="createproject" value="Ajouter un projet" />';
	if (file_exists($basepath.'/'.$_CONFIG['template'])) {
		echo '<input type="button" id="createprojecttemplate" value="Ajouter selon modèle" />';
	}
	echo '</td>';
echo '</tbody>';
echo "</table>
	</div>";
?>
