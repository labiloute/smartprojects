<?php

class Project {
	public function __construct($_CONFIG,$Task)
		{
		$this->CONFIG=$_CONFIG;
		if ($Task)
			$this->Task=$Task;
		else {
			include_once('task.php');
			$this->Task=new Task;
			}
		// Init adresse book
		$this->pabook=array();
		}

	// Append data to adress book
	function SetABook($people) {
		if (count($people)==0 or $people=="") return False;
		else if (is_array($people)){$people=array_filter($people); $this->pabook=array_merge($people,$this->pabook);}
		else array_push($this->pabook,$people);
		}

	function displayproject($pcount,$ppath) {

		$tmstp=time();

		// Init
		$cbar='';

		// Process ini file with section
		$pfile=$ppath."/".$this->CONFIG['pfile'];

		(file_exists($pfile))?$pconfig=parse_ini_file($pfile,True):$pconfig=False;

		// Remove full path
		$pdirname=explode('/',$ppath);
		$pdirname=end($pdirname);

		// Explode directory name
		$pdirname_exploded=explode('-',$pdirname);

		// Get Project Name without priority
		$pdirnameWOprio=$pdirname_exploded;
		array_shift($pdirnameWOprio);
		$pdirnameWOprio=implode('-',$pdirnameWOprio);

		// Bypass project with priority 0
		$ppriority=intval($pdirname_exploded[0]);
		if ($ppriority == 0) return False;

		// Remove priority from dir name

		unset($pdirname_exploded[0]);
		// Get project name
		$pname=str_replace('_',' ',implode('-',$pdirname_exploded));

		// Pourquoi est-ce qu'on provilegie le pcount ? Je ne me souviens plus !
		//$pid = $pcount.'-'.str_replace(' ','-',$pname);

		// Usage de la priorité indispensable pour :
		// - le Scrollage vers le projet en JS ( voir fonction ScrollTo) 
		// -
		$pid = $ppriority.'-'.str_replace(' ','-',$pname);

		// Done ?
		($pconfig['Main']['done'])?$css_done='done':$css_done='';

		/****************************/
		// Priority *****************/
		//***************************/

		$htmlpriority='<select name="ppriority" class="ppriority">';
		for ($z=1;$z<100;$z++) {
			($z==$ppriority)?$sel='selected="selected"':$sel='';
			$htmlpriority.='<option value="'.$z.'" '.$sel.' >'.$z.'</option>';
			}
		$htmlpriority.='</select>';


		/***************************/
		// DISPLAY *****************/
		//**************************/

		// Colore si présent au Schéma directeur
		$pbuttons='';$c_dpi='';$t_dpi='';$t_masterplan='';$c_masterplan='';
		if ($pconfig['FHM']['masterplan']=='1') {$c_masterplan.='masterplan';$t_masterplan.='Projet inclut au schéma directeur en cours';}else{$t_masterplan.='Inclure le projet au schéma directeur.';}
		// Colore si DPI
		if ($pconfig['FHM']['dpi']=='1') {$c_dpi.='dpi';$t_dpi.='Projet concernant le DPI';}else{$t_dpi.='Indiquer que le projet concerne le DPI.';}


		// Construct buttons
		$button['setdone'] = '<span class="setdone circled '.$css_done.'" title="Projet terminé /  Non terminé">&#10003;</span>';
		$button['setmasterplan'] = '<span class="setmasterplan circled '.$c_masterplan.'" title="'.$t_masterplan.'">&#9962;</span>';
		$button['setdpi'] = '<span class="setdpi circled '.$c_dpi.'" title="'.$t_dpi.'">&#127973;</span>';
		$button['refresh'] = '<span class="refresh circled" title="Rafraîchir">&#8634;</span>';
		$button['archive'] = '<span class="archive circled" title="Archiver">&#129512;</span>';
		$button['toggletaskdone']='<span class="toggletaskdone circled" title="Afficher ou masquer temporairement les tâches terminées">&#9745;</span>';
		$button['datechanger']='<br /><select class="datechangeroption">
					<option value="">- Date à changer- </option>
					<option value="start">Date de démarrage du projet</option>
					<option value="deadline">Deadline</option>
					<option value="done">Date de réalisation du projet</option>
				</select>
				<input type="text" class="datechangerdate datepicker" value="" size="8" />';
		// Order buttons the good way
		foreach ($button as $key=>$but) {
			if (in_array($key,$this->CONFIG['buttons']['project']))
				$buttons_1.=$but;
			else
				$buttons_2.=$but;
			}

		// Boutons projet
		//$pbuttons.='<span class="setdone circled '.$css_done.'" title="Projet terminé /  Non terminé">&#10003;</span>';
		//$pbuttons.='<span class="setmasterplan circled '.$c_masterplan.'" title="'.$t_masterplan.'">&#9962;</span>';
		//$pbuttons.='<span class="setdpi circled '.$c_dpi.'" title="'.$t_dpi.'">&#127973;</span>';
		//$pbuttons.='<span class="refresh circled">&#8634;</span>';
		//$pbuttons.='<span class="toolsbutton circled" title="Plus d\'outils">+<!--&#128295;--></span>';
		$pbuttons.=$buttons_1;
		if ($buttons_2) $pbuttons.='<span class="toolsbutton circled" title="Plus d\'outils">+<!--&#128295;--></span>';
		if ($buttons_2)	$pbuttons.='<div id="'.$pid.'-tools" class="tools" style="display:none;">';
		if ($buttons_2)	$pbuttons.=$buttons_2;
		if ($buttons_2)	$pbuttons.='</div>';

		/*
		'		<select class="datechangeroption">
					<option value="">- Date à changer- </option>
					<option value="start">Date de démarrage du projet</option>
					<option value="deadline">Deadline</option>
					<option value="done">Date de réalisation du projet</option>
				</select>
				<input type="text" class="datechangerdate datepicker" value="" size="8" />
			</div>';
		*/
		//*************************/
		// Gestion de la deadline */
		//*************************/
		$deadlinetmstp = strtotime($pconfig['Main']['deadline']);
		$difftmstp = round(($deadlinetmstp - $tmstp)/86400,0)+1;
		if ($pconfig['Main']['deadline']=='') {$cdeadline='nodeadline';$txtdeadline='<input type="text" value="&#128197;" class="deadlinepicker datepicker" size="8" />';$tdeadline='Aucune deadline saisie dans le fichier projet.';}
		else if ($difftmstp<0){$cdeadline='exceeded';$txtdeadline='<input type="text" size="8" value="'.$pconfig['Main']['deadline'].'" class="deadlinepicker datepicker" /><br /><span class="discret1">il y a '.abs($difftmstp).' jours</span>';$tdeadline='Deadline dépassée';}
		else {$cdeadline='';$tdeadline='';$txtdeadline='<input type="text" size="8" value="'.$pconfig['Main']['deadline'].'" class="deadlinepicker datepicker" /><br /><span class="discret1">dans '.$difftmstp.' jours</span>';}

		// Conversion de la date de début en timestamp
		$starttmstp=strtotime($pconfig['Time']['start']);

		// Avancement
		if ($pconfig['Main']['deadline']!='' and $pconfig['Time']['start'] != '') {
			$projectduration = 1+round(($deadlinetmstp - $starttmstp)/86400,0);
			$cbar='bar';
			// In case of datestart=dateend
			if ($projectduration<=0) {
				$projectduration=1;
				$durationwarning=' Attention, la date de fin de projet ('.$pconfig['Time']['start'].') est antérieure ou égale à la date de début de projet ('.$pconfig['Main']['deadline'].').';
				}
			// Case today is included in project period
			if ($tmstp <= $deadlinetmstp) {
				$theoricalratio=100-round($difftmstp/$projectduration*100,0);
				$ttheoricalratio=$difftmstp.' jours restant sur '.$projectduration.' jours prévus.'.$durationwarning.'';
				}
			// Case today is out off project period
			else if ($tmstp > $deadlinetmstp) {
				$theoricalratio=abs(100+round($difftmstp/$projectduration*100,0));
				$ttheoricalratio=abs($difftmstp).' jour(s) de retard. '.$projectduration.' jour(s) étaient prévus&#13;(Début de projet '.$pconfig['Time']['start'].' et deadline : '.$pconfig['Main']['deadline'].')';
				}
			}
		else
			{
			$projectduration=0;
			$ttheoricalratio=0;
			$theoricalratio=0;
			$ttheoricalratio='Il manque la date de début ou la deadline.';
			}

		//*********************************/
		// Commentaire			 **/
		//*********************************/
		$comment=htmlentities(preg_replace('/[^A-Za-z0-9\-]/', ' ',$pconfig['Main']['comment']));
		if ($comment!="") {
			$html_comment='&nbsp;<span class="commentnotification circled active" title="'.$comment.'">?</span>';
			}
		else $html_comment='&nbsp;<span class="commentnotification circled" title="Cliquez pour insérer un commentaire">?</span>';


		//*******************************/
		// Gestion de la fin de projet **/
		//*******************************/
		if ($pconfig['Main']['done'] != '') {
			$realendtmstp=strtotime($pconfig['Main']['done']);
			if ($starttmstp) {
				$finishedin = round(($realendtmstp - $starttmstp)/86400,0);
				$finishedtxt = 'Projet terminé le '.$pconfig['Main']['done'].' en '.$finishedin.' jours';
				}
			else {
				$finishedin=True;
				$finishedtxt = 'Projet terminé le '.$pconfig['Main']['done'];
				}
			}
		else
			$finishedin=False;


		//*******************************/
		// Manager                     **/
		//*******************************/
		$txtmanager='';
		if (strpos($pconfig['Main']['manager'],',')) {
			$managers=explode(',',$pconfig['Main']['manager']);
			}
		else $managers = array($pconfig['Main']['manager']);
		$k=0;
		foreach ($managers as $manager) {
			if ($k!=0)$txtmanager.='<br />';
			if (strpos($manager,'@')) {$txtmanager.=explode('@',$manager)[0];}
			else $txtmanager.=$manager;
			$k++;
			}

		// Store this in local project adress book
		$this->SetABook($managers);

		//*******************************/
		// Team                     **/
		//*******************************/
		$txtteam='';
		if (strpos($pconfig['Main']['team'],',')) {
			$team=explode(',',$pconfig['Main']['team']);
			}
		else $team = array($pconfig['Main']['team']);
		$k=0;
		foreach ($team as $teammember) {
			if ($k!=0)$txtteam.='<br />';
			if (strpos($teammember,'@')) {$txtteam.=explode('@',$teammember)[0];}
			else $txtteam.=$teammember;
			$k++;
			}
		// Store this in local project adress book
		$this->SetABook($team);

		//*******************************/
		// Affichage de la ligne       **/
		//*******************************/
		echo '<tr id="'.$pid.'" class="project" dirname="'.$pdirname.'" project="'.$pname.'" pfile="'.$pfile.'" start="'.$pconfig['Time']['start'].'" pcount="'.$pcount.'">';
			// Project Name
			echo '<td class="pname" colspan="2">
					<span class="circled projectnameicon" title="Cliquez pour renommer le projet">&#128458;</span>
					<input type="text" class="projectnameinput" style="display:none;" value="'.$pdirnameWOprio.'" >
					'.$pname.'&nbsp;&nbsp;&nbsp;
					<span class="circled-wrapper" id="taskcount-wrapper-'.$pcount.'" style="display:none;">
						<span class="circled" id="taskcount-'.$pcount.'" title="Nombre de tâches du projet" "pcount="'.$pcount.'"></span>
					</span>
					'.$html_comment.'
					<input type="text" class="commentinput" style="display:none;" title="'.$comment.'" value="'.$comment.'" >
				</td>';
			//echo "<td>".$pconfig['Main']['goal']."</td>";
			// Priority (Ranking)
			echo "<td>".$htmlpriority."</td>";
			// If not finished yet
			if ( !$finishedin) {
				echo '<td class="'.$cdeadline.' firstprojectconfig" title="'.$tdeadline.'">'.$txtdeadline.'</td>';
				echo '<td class="projectmanager projectconfig">
						<span class="mlabel">'.$txtmanager.'</span>
						<input size="10" type="text" class="manager autocomplete" style="display:none;" value="'.$pconfig['Main']['manager'].'" />';
				echo "</td>";
				echo '<td class="projectteam projectconfig">
					<span class="teamlabel">'.$txtteam.'</span>
					<input size="10" type="text" class="team autocomplete" style="display:none;" value="'.$pconfig['Main']['team'].'" />';
				echo '</td>';
				echo '<td class="projectcost projectconfig">
					<span class="costlabel"></span> <span class="costcurrency" style="display:none;">'.$this->CONFIG['currency'].'</span>';
				echo '</td>';
				echo '<td title="'.$ttheoricalratio.'" class="projectconfig">
					<div class="progress-wrapper">
						<div class="bar-container">
							<div class="'.$cbar.'" id="bar-'.$pcount.'"></div>
						</div><div class="value" percent="'.$theoricalratio.'">'.$theoricalratio.'</div>
						</div>
					</td>';
				echo '<td class="nextstep" class="projectconfig"></td>';
			}
			// Elise if finished
			else if ($finishedin) {
				echo '<td class="finished" colspan="6">'.$finishedtxt.'</td>';
				}
			echo '<td id="'.$pid.'-buttons">'.$pbuttons.'</td>';
			/*
			else {
				echo '<td class="discret1" colspan="4">Fichier .ini absent du répertoire</td>';
				echo '<td id="nextstep-'.$pcount.'"></td>';
				echo '<td>'.$pbuttons.'</td>';
				}
			*/
		echo "</tr>";
		// Fin de ligne

		// ********************************************************************************************************************/
		// TASKS **************************************************************************************************************/
		// *******************************************************************************************************************/

		// Count tasks
		$taskcount=1;

		// Loop thru each task in project
		foreach(glob($ppath.'/[0-9][0-9]*', GLOB_ONLYDIR) as $taskpath)
			{
			$rtask=$this->Task->displaytask($pid,$ppriority,$pcount,$taskcount,$ppath,$pname,$taskpath);
			if (! $rtask) continue;
			$taskcount++;
			// Store task adress book in local project object
			$this->pabook=array_merge($this->pabook,$this->Task->tabook);
			}
		// Deduplicate adress book
		$this->pabook=array_unique($this->pabook);

		// *******************/
		// New Task form *******/
		// *******************/

		$htmlpriority='<select name="newtask_priority" id="newtask_priority" class="new_priority">';
		for ($z=1;$z<100;$z++) {
			($z==50)?$sel='selected="selected"':$sel='';
			$htmlpriority.='<option value="'.$z.'" '.$sel.' >'.$z.'</option>';
			}
		$htmlpriority.='</select>';
		echo '<tr class="task" project="'.$pname.'" pid="'.$pid.'" style="display:none;">';
		echo '<td colspan="2"><input id="newtask_name" placeholder="Nom_de_la_tache" pattern="[a-zA-Z0-9_-]+"/></td>';
		echo '<td>'.$htmlpriority.'</td>';
		echo '<td colspan="7"><input type="button" class="createtask" value="Ajouter une tâche" /></td>';
		echo '</tr>';

		// Doit retourner True à la fin si tout s'est bien passé
		return True;
		}
	}

?>
