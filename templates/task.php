<?php
// ********************************************************************************************************************/
// TASKS **************************************************************************************************************/
// *******************************************************************************************************************/

/*
 Elements à surveiller :
- $taskcount (fait partie du taskid)
- $pcount (identifiant de ligne projet, fait partie du taskid)
- $project, le nom du projet
- $pname, le nom du projet (?)
- $path, le chemin du fichier ini des tasks
*/



class Task {
	public function __construct($_CONFIG) {
		$this->CONFIG=$_CONFIG;
		$this->tabook=array();
		}

	function SetABook($people) {
		if (count($people)==0 or $people=="") return False;
		else if (is_array($people)){$people=array_filter($people); $this->tabook=array_merge($people,$this->tabook);}
		else array_push($this->tabook,$people);
		//print_r($this->tabook);print('<br />');
		}

	function displaytask($pid,$ppriority,$pcount,$taskcount,$project, $pname,$path)
	{
		$tmstp=time();
		//echo $pcount.'<br />';
		$tfile=$path."/".$this->CONFIG['tfile'];
		(file_exists($tfile))?$taskconfig=parse_ini_file($tfile,True):$taskconfig=False;

		// Remove full path
		$taskdirname=explode('/',$path);
		$taskdirname=end($taskdirname);
		// Explode directory name
		$taskdirname=explode('-',$taskdirname);

		// Get Project Name without priority
		$tdirnameWOprio=$taskdirname;
		array_shift($tdirnameWOprio);
		$tdirnameWOprio=implode('-',$tdirnameWOprio);


		/****************************/
		// Priority *****************/
		//***************************/

		// Extract priority
		$taskpriority=intval($taskdirname[0]);

		// Ignore line with priority = 0
		if ($taskpriority == 0) return False;

		// Construct Select box
		$htmlpriority='<select name="tpriority" class="tpriority">';
		for ($z=1;$z<100;$z++) {
			($z==$taskpriority)?$sel='selected="selected"':$sel='';
			$htmlpriority.='<option value="'.$z.'" '.$sel.' >'.$z.'</option>';
			}
		$htmlpriority.='</select>';

		// Remove priority from dir name
		unset($taskdirname[0]);
		// Get task name
		$taskname=str_replace('_',' ',implode('-',$taskdirname));
		// Is Task already done ?
		if ($taskconfig['Main']['done']!='' and $taskconfig['Main']['done']!='0') {$cssdone='done';$taskdone=True;} else {$cssdone='';$taskdone=False;}
		// Is task part of critical path ?
		($taskconfig['Main']['critical']!='' and $taskconfig['Main']['critical']!='0')?$taskcritical=True:$taskcritical=False;


		// Stop here if hiding enabled
		($_SESSION['showdone']===False)?$hidedone=True:$hidedone=False;
		
		($taskdone && $hidedone)?$css_hide='hide':$css_hide='';

		/***************************/
		// DISPLAY *****************/
		//**************************/
		// Init
		$html_progress='';
		// Dernier relance (last reminder)
		if ($taskconfig['Time']['last_reminder']!="") {
			$tlast_reminder='Derniere relance le '.$taskconfig['Time']['last_reminder'];
			}
		else $tlast_reminder="";

		// Gestion de la deadline
		$starttmstp=strtotime($taskconfig['Time']['start']);
		$endtmstp = strtotime($taskconfig['Main']['done']);
		$deadlinetmstp = strtotime($taskconfig['Main']['deadline']);
		// Relative date depends of if task is finished or not
		if($taskconfig['Main']['done'])
			$relativeendtmstp=strtotime($taskconfig['Main']['done']);
		else		
			$relativeendtmstp=$tmstp;

		$diffdays = round(($relativeendtmstp-$deadlinetmstp)/86400,0);
		$tasktheoricalduration = 1+round(($deadlinetmstp - $starttmstp)/86400,0);
		$tasktheoricalratio=abs(100+round($diffdays/$tasktheoricalduration*100,0));
		$ctaskbar='bar';
		if ($taskconfig['Main']['deadline']=='') {$ctaskdeadline='nodeadline';$txttaskdeadline='<input type="text" size="8" value="&#128197;" class="deadlinepicker datepicker" />';$ttaskdeadline='Aucune deadline saisie dans le fichier de la tache.';}
		else if ($diffdays>0){$ctaskdeadline='deadline-exceeded';$txttaskdeadline='<input type="text" size="8" value="'.$taskconfig['Main']['deadline'].'" class="deadlinepicker datepicker" /><br /><span class="discret1">il y a '.abs($diffdays).' jours</span>';$ttaskdeadline='Deadline de tâche dépassée';}
		else {$ctaskdeadline='';$ttaskdeadline='';$txttaskdeadline='<input size="8" type="text" value="'.$taskconfig['Main']['deadline'].'" class="deadlinepicker datepicker" /><br /><span class="discret1">dans '.abs($diffdays).' jours</span>';}
		//***********************/
		//** Avancement relatif */
		//***********************/

		// Tâche en cours
		if ($taskconfig['Main']['deadline']!='' and $taskconfig['Time']['start'] != '' and ! $taskconfig['Main']['done']) {
			// Task on time
			if ($tmstp <= $deadlinetmstp) {
				$ttasktheoricalratio=abs($diffdays).' jour(s) restant sur '.$tasktheoricalduration.' jours prévus&#13;(Début de tâche '.$taskconfig['Time']['start'].' et deadline : '.$taskconfig['Main']['deadline'].')';
				}
			// Task late
			else {
				$ttasktheoricalratio=abs($diffdays).' jour(s) de retard. '.$tasktheoricalduration.' jour(s) étaient prévus&#13;(Début de tâche '.$taskconfig['Time']['start'].' et deadline : '.$taskconfig['Main']['deadline'].')';
				$html_progress=abs($diffdays).' day(s) late';
				$ctaskbar='bar white';
				}
			}
		// Tâche finished
		else if ($taskconfig['Main']['deadline']!='' and $taskconfig['Time']['start'] != '' and $taskconfig['Main']['done']) {
			// Task finished earlier
			if ($endtmstp <= $deadlinetmstp) {
				$ttasktheoricalratio ='Tâche terminée '.abs($diffdays).' jour(s) en avance (sur '.$tasktheoricalduration.' prévus)&#13;Début de tâche '.$taskconfig['Time']['start'].', terminée le '.$taskconfig['Main']['done'].' au lieu du '.$taskconfig['Main']['deadline'].'';
				$ctaskbar='bar white';
				}
			// Task finished late
			else {
				$ttasktheoricalratio ='Tâche terminée avec '.abs($diffdays).' jour(s) de retard (sur '.$tasktheoricalduration.' prévus)&#13;Début de tâche '.$taskconfig['Time']['start'].', terminée le '.$taskconfig['Main']['done'].' au lieu du '.$taskconfig['Main']['deadline'].'';
				$ctaskbar='bar white';$html_progress=abs($diffdays).' day(s) late';
				}
			}
		// Task which lacks some elements
		else
			{
			$tasktheoricalduration=0;
			$ttheoricalratio=0;
			$tasktheoricalratio=0;
			$ttasktheoricalratio='Il manque ou la date de début (ou la deadline) dans le fichier de tâche.';
			}
		// Inconsistent data
		if ($taskconfig['Main']['deadline']=='' or $taskconfig['Time']['start']=='' or $tasktheoricalratio<0) {$ctaskbar='';}
		//***********************/
		//** Manager(s)        **/
		//***********************/
		$txttaskmanager='';
		if (strpos($taskconfig['Main']['manager'],',')) {
			$taskmanagers=explode(',',$taskconfig['Main']['manager']);
			}
		else $taskmanagers = array($taskconfig['Main']['manager']);

		// Update Adress Book cache
		$this->SetABook($taskmanagers);

		// Format Manager for display
		$k=0;
		foreach ($taskmanagers as $taskmanager) {
			if ($k!=0)$txttaskmanager.='<br />';
			if (strpos($taskmanager,'@')) {$txttaskmanager.=explode('@',$taskmanager)[0];}
			else $txttaskmanager.=$taskmanager;
			$k++;
			}

		// Team
		$txtteam='';
		if (strpos($taskconfig['Main']['team'],',')) {
			$team=explode(',',$taskconfig['Main']['team']);
			}
		else $team = array($taskconfig['Main']['team']);

		// Update Adress Book cache
		$this->SetABook($team);

		// Prepare Team members for display
		$k=0;
		foreach ($team as $teammember) {
			if ($k!=0)$txtteam.='<br />';
			if (strpos($teammember,'@')) {$txtteam.=explode('@',$teammember)[0];}
			else $txtteam.=$teammember;
			$k++;
			}

		// Cost
		$txtcost=$taskconfig['Main']['cost'];
		($txtcost=="")?$ccurrency='display:none;':$ccurrency='';

		// Commentaire
		$comment=htmlentities(preg_replace('/[^A-Za-z0-9\-]/', ' ', $taskconfig['Main']['comment']));
		($comment!="")?$css_comment='comment':$css_comment="";


		// Task id
		$taskid=$pid.'-'.$taskcount;

		// Boutons
		($taskdone)?$css_done='done':$css_done='';
		if ($taskcritical) {$css_critical='critical';}
		else {$css_critical='notcritical';}
		$button['settaskdone']='<span class="setdone circled '.$css_done.'" title="Tâche réalisée /  Non réalisée">&#10003;</span>';
		$button['setcritical']='<span class="setcritical circled '.$css_critical.'" title="Chemin critique / Hors chemin critique">&#10175;</span>';
		$button['taskemail']='<span class="taskemail circled '.$css_taskemail.'" title="Envoyer une relance">&#x2709;</span>';
		$button['noticemanager']='<span class="noticemanager circled '.$css_taskemail.'" title="Notifier le manager qu\'il est responsable de la tâche">&#128483;</span>';

		$button['datechanger']='<br /><select class="datechangeroption">
					<option value="">- Date à changer- </option>
					<option value="last_reminder">Dernière relance</option>
					<option value="done">Date de réalisation de la tâche</option>
					<option value="start">Date de démarrage de la tâche</option>
					<option value="deadline">Deadline</option>
				</select>
				<input type="text" class="datechangerdate datepicker" value="" size="8" />
				';

		// Order buttons the good way
		foreach ($button as $key=>$but) {
			if (in_array($key,$this->CONFIG['buttons']['task']))
				$buttons_1.=$but;
			else
				$buttons_2.=$but;
			}

		$tbuttons=$buttons_1;
		if ($buttons_2) $tbuttons.='<span class="toolsbutton circled" title="Plus d\'outils">+<!--&#128295;--></span>';
		if ($buttons_2) $tbuttons.='<div id="'.$taskid.'-tools" class="tools" style="display:none;">';
		if ($buttons_2) $tbuttons.=$buttons_2;
		if ($buttons_2) $tbuttons.='</div>';

		// Affichage de la ligne
		echo '<tr class="task '.$cssdone.' '.$css_hide.'" id="'.$taskid.'" tfile="'.$tfile.'" project="'.$pname.'" pid="'.$pid.'" start="'.$taskconfig['Time']['start'].'" taskcount="'.$taskcount.'" style="display:none;">';
			echo '<td class="blank">';
			echo '<span class="circled '.$css_comment.' commentbullet">&#8627;</span>';
			echo '<input type="text" class="commentinput" style="display:none;" value="'.$comment.'" />';
			echo '</td>';
			echo '<td class="taskname" title="'.$comment.'">
					<span class="tasknamevalue">'.$taskname.'</span>
					<input type="text" class="tasknameinput" style="display:none;" value="'.$tdirnameWOprio.'"  pattern="[a-zA-Z0-9_-]+" >
				</td>';
			//echo "<td>".$taskconfig['Main']['goal']."</td>"; 
			echo "<td>".$htmlpriority."</td>";
			// Trois colonnes dépendant de la tâche
			//if ( ! $taskdone) {
				echo '<td class="firsttaskconfig '.$ctaskdeadline.' deadline" title="'.$ttaskdeadline.'">'.$txttaskdeadline.'</td>';
				echo '<td class="taskconfig taskmanager ">
					<span class="mlabel">'.$txttaskmanager.'</span>
					<input size="10" type="text" class="manager autocomplete" style="display:none;" value="'.$taskconfig['Main']['manager'].'" /><br />
					<span class="discret2">'.$tlast_reminder.'</span>
					</td>';
				echo '<td class="taskconfig taskteam">';
				echo '	<span class="teamlabel">'.$txtteam.'</span>
					<input size="10" type="text" class="team autocomplete" style="display:none;" value="'.$taskconfig['Main']['team'].'" />';
				echo '</td>';
				echo '<td class="taskconfig taskcost">';
				echo '	<span class="costlabel">'.$txtcost.'</span>
					&nbsp;<span class="costcurrency" style="'.$ccurrency.'">'.$this->CONFIG['currency'].'</span>
					<input size="10" type="text" class="cost" style="display:none;" value="'.$txtcost.'" />';
				echo '</td>';

				echo '<td class="taskconfig progress" title="'.$ttasktheoricalratio.'">
						<div class="progress-wrapper">
							<div class="bar-container">
								<div class="'.$ctaskbar.'" id="bar-'.$taskcount.'">
								'.$html_progress.'
								</div>
							</div>
							<div class="value" percent="'.$tasktheoricalratio.'">'.$tasktheoricalratio.'</div>
						</div>
					</td>';
				echo '<td class="taskconfig"></td>';
			// Colonne action
			echo '<td title="Actions" class="actions" id="'.$taskid.'-buttons" pid="'.$pid.'" taskid="'.$taskid.'">';
			echo $tbuttons;
			echo '</td>';
			
		echo "</tr>";
		// Doit retourner True à la fin si tout s'est bien passé
		return True;
		}

	}
?>
