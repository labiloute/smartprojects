<?php session_start(); ?>
<!-- Include html header -->
<?php include_once("include/header.php"); ?>
<!-- Include Config -->
<?php
	if (file_exists("include/config.default.php")) include_once("include/config.default.php");
	if (file_exists("include/config.php")) include_once("include/config.php");
	else {echo '<div class="config-file-error"><strong>Smart Projects - Small an efficient project tool</strong><br />Strongly recommanded : please create custom config file (config.php) into the include directory, by copying the default one (config.default.php)</div>';exit;}
?>
<!-- Body -->
<body>
<!-- Include Menu -->
<?php if ($_CONFIG['show_menu']) include_once("include/menu.php"); ?>
<!-- Include Ldap class -->
<?php
	if ($_CONFIG['custom_ldap_class'] and file_exists($_CONFIG['custom_ldap_class'])) include_once($_CONFIG['custom_ldap_class']);
	else include_once("include/ldap.class.php");
?>
<!-- Include Ldap class -->
<?php include_once("include/mail.php"); ?>
<!-- Include Mysql class -->
<?php include_once("include/mysql.php"); ?>

<!-- Internal classes -->
<?php include_once("templates/task.php"); ?>
<?php include_once("include/acl.php"); ?>


<!-- LDAP CX and cache -->
<?php
$ldap = new Ldap($_CONFIG);
if ($_CONFIG['ad_host'] and ! $_CONFIG['noauth']) {
	$ldap->connect();
	$username=$ldap->getUsername($_SERVER['REMOTE_USER']);
	$attr=$ldap->getAttributes($username,array('mail','givenname','sn'));
	($attr['mail'][0]!=NULL)?$useremail=$attr['mail'][0]:$useremail=NULL;
	//if ($useremail!=NULL){$s_mail = explode('@',$useremail);$useremail=$s_mail[0];}

	($attr['sn'][0]!=NULL && $attr['mail'][0]!=NULL)?$userlastname=$attr['sn'][0]:$userlastname=NULL;
	($attr['givenname'][0]!=NULL && $attr['mail'][0]!=NULL)?$userfirstname=$attr['givenname'][0]:$userfirstname=NULL;
	$useremail=$attr['mail'][0];
	// Mis en cache des groupes d'appartenance
	if (!$_SESSION['groups'])
		$groups = $ldap->getGroups($username);
	}
?>
<!-- / LDAP CX -->

<!-- ACL -->
<?php
// Reset session value if requested
if ($_GET['logout'])unset($_SESSION);
// Create ACL checker object
$Acl = new Acl($_CONFIG);
// Init profile var (RW/RO)
$profile=$Acl->GetProfile($groups);
//echo $_SESSION['profile'];
?>
<!-- / ACL -->
<?php
$tmstp=time();
include_once('include/header.php');
?>

<?php if ($_SESSION['profile']) include_once("templates/projects.php"); ?>

</body>
<!-- Include footer -->
<?php include_once("include/footer.php"); ?>
