// Const
function getQueryParams(qs) {
    qs = qs.split("+").join(" ");
    var params = {},
        tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])]
            = decodeURIComponent(tokens[2]);
    }

    return params;
}

function getPathFromUrl(url) {
  return url.split("?")[0];
}

function dateDiffInDays(a, b) {
	// Discard the time and time-zone information.
	var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
	var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
	return Math.floor((utc2 - utc1) / _MS_PER_DAY);
	}


// Start Jquery
$(document).ready(function()
	{
	$('#projects-table').stickyTableHeaders();
	/****************************************/
	/************* SETTINGS *****************/
	/****************************************/

	// Set DatePicker Format
	$('body').on('focus','input[class*=\'datepicker\']', function(){
	    $(this).datepicker(tpregional['fr']);
	});

	// retrieve get data
	var get = getQueryParams(document.location.search);

	// French language options
	var tpregional = {};
	tpregional['fr'] = {
			dateFormat: "dd-mm-yy", 							
			timeFormat:"HH:mm",
			currentText:'Maintenant',
			closeText:'OK',
			stepMinute:5,
			//altField: "#datepicker",
			closeText: 'Fermer',
			prevText: 'Précédent',
			nextText: 'Suivant',
			monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
			monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
			dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
			dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
			dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
			weekHeader: 'Sem.',
			//dateFormat: 'yy-mm-dd',
			firstDay : 1,
			// Timepicker
			timeOnlyTitle: 'Choisir une heure',
			timeText: 'Heure',
			hourText: 'Heures',
			minuteText: 'Minutes',
			secondText: 'Secondes',
			millisecText: 'Millisecondes',				
			timezoneText: 'Fuseau horaire',
			currentText: 'Maintenant',
			amNames: ['AM', 'A'],
			pmNames: ['PM', 'P'],
			//ampm: false
			};
	/****************************************/
	/************* FUNCTIONS ****************/
	/****************************************/

	function ShowTasks(pid=null,pname=null) {
		// Show up
		if (pid) {
			console.log("Showing up all tasks with pid "+pid+".")
			$('tr.task[pid='+pid+']').not('.hide').show("slow");
			//$('tr.task.done.hide[pid='+pid+']').hide();
			}
		else if (pname) {
			console.log("Showing up all tasks with pname "+pname+".")
			$('tr.task[project='+pname+']').not('.hide').show("slow");
			//$('tr.task.done.hide[project='+pname+']').hide();
			}
		}

	// Copier Total tasks count to Projet TR  - UNUSED ANYMORE - COULD BE DELETED
	function filltaskcount() {
		$('.task').each(function(i, obj) {
			var taskcount = $(this).attr( "taskcount" );
			var pid = $(this).attr( "pid" );
			//console.log("Alimentation du nombre de tâches ("+taskcount+") du projet "+pid+".");
			$('tr#'+pid+' td.pname span.circled-wrapper span.circled').html(taskcount);
			if (taskcount>0) {$('tr#'+pid+' td.pname span.circled-wrapper').show();}
		});
	}

	// Pull up manager of last critical task to projet line  - UNUSED ANYMORE - COULD BE DELETED
	function fillnextstep(element=null) {
		if (element==null) element='.task';
		else element='.task[pid='+element+']';
		$(element).each(function(i, obj) {
			// Get task id
			taskid=$(this).attr('id');
			// Get project PID (from task line)
			var pid = $(this).attr( "pid" );
			// Task should be critical
			var iscritical = $(this).find("span.setcritical").hasClass("critical");
			// Get HTML content of nextstep cell
			var nextstep = $('tr#'+pid+' td.nextstep').html();
			// Get percentage of finished value
			percent=$(this).find('td.taskconfig.progress > div.progress-wrapper > div.value').attr('percent');
			// Task has not to be done
			var isdone = $(this).hasClass("done");
			//console.log("Task is done : "+isdone);
			// Fill nextstep if task is critical and that netstep is not already filled yet
			if (iscritical && nextstep=='' && ! isdone) {
				//console.log("Cette tâche est la prochaine tache critique du projet "+pid);
				//var manager = $(this).find("td.taskmanager").justtext();
				var manager = $(this).find("td.taskmanager .mlabel").html();
				//console.log("["+taskid+"] Remplissage de nextstep du projet "+pid+" avec "+manager+" (tâche terminée à "+percent+" %)");

				$('tr#'+pid+' td.nextstep').html(manager);
				// Task percentage
				if (percent > 0 && percent < 70) cssprct='ok';
				else if (percent > 70 && percent < 100) cssprct='warning';
				else if (percent==0 || percent >= 100) cssprct='error';
				else cssprct='';
				//
				if (percent!=undefined && percent > 0) {
					$('tr#'+pid+' td.nextstep').prop('title',"Tâche a "+percent+" % de sa deadline.");
					}
				else $('tr#'+pid+' td.nextstep').prop('title',"Pas de deadline sur cette tâche !");
				// Colorize
				$('tr#'+pid+' td.nextstep').removeClass('ok').removeClass('warning').removeClass('error').addClass(cssprct);
				}
			//if (taskcount>0) {$('#taskcount-wrapper-'+pcount).show();}
		});
	}


	// Calculate project total cost
	function fillprojectcost(element=null) {
		if (element==null) element='.task';
		else element='.task[pid='+element+']';
		var totalcost=0;
		$(element).each(function(i, obj) {
			var taskcount = $(this).attr( "taskcount" );
			//console.log(taskcount);
			if (taskcount=="1") {
				totalcost=0;
				}
			// Get task id
			taskid=$(this).attr('id');
			// Get project PID (from task line)
			var pid = $(this).attr( "pid" );
			// Task should be critical
			// Get HTML content of nextstep cell
			var cost = $(this).find("td.taskcost input.cost").val();
			cost=Number(cost);
			//console.log("COUT : "+cost);
			if (cost && typeof cost === "number" && cost != "") {
				totalcost += cost;
				console.log("COUT TOTAL : "+totalcost);
				// Fill cost
				console.log("["+taskid+"] Filling cost of project "+pid+" with "+totalcost+"");
				$('tr#'+pid+' td span.costlabel').html(totalcost);
				$('tr#'+pid+' td span.costcurrency').show();
				}
			else if (totalcost==0) {
				$('tr#'+pid+' td span.costlabel').html("");
				$('tr#'+pid+' td span.costcurrency').hide();
				}
			});
		}

	jQuery.fn.justtext = function() {
		return $(this)	.clone()
				.children()
				.remove()
				.end()
				.text();
		};
		

	/***********************************/
	/****** UNIFIED JS FOR EACH TASK ***/
	/***********************************/

	function unifiedtask(element=null) {
		//if (element==null) element=$('.task');
		//else element=$('.task[pid='+element+']');
		if (element==null) element='.task';
		else element='.task[pid='+element+']';

		var totalcost=0;
//		var element = $(element); //collect all spans as array
		
		$(element).each(function(i, obj) {
                //for (var i = 0, len = element.length; i < len; i++) { //for loop
		//$('.task').each(function(i, obj) {

			//*******************************/
			// DISPLAY TASK COUNT
			var taskcount = $(this).attr( "taskcount" );
			var pid = $(this).attr( "pid" );
			//console.log("Alimentation du nombre de tâches ("+taskcount+") du projet "+pid+".");
			$('tr#'+pid+' td.pname span.circled-wrapper span.circled').html(taskcount);
			if (taskcount>0) {$('tr#'+pid+' td.pname span.circled-wrapper').show();}
			
			
			
			//*******************************/
			// TOTAL COST OF PROJECT
			var taskcount = $(this).attr( "taskcount" );
			//console.log(taskcount);
			if (taskcount=="1") {
				totalcost=0;
				}
			// Get task id
			taskid=$(this).attr('id');
			// Get project PID (from task line)
			var pid = $(this).attr( "pid" );
			// Task should be critical
			// Get HTML content of nextstep cell
			var cost = $(this).find("td.taskcost input.cost").val();
			cost=Number(cost);
			//console.log("COUT : "+cost);
			if (cost && typeof cost === "number" && cost != "") {
				totalcost += cost;
				console.log("COUT TOTAL : "+totalcost);
				// Fill cost
				console.log("["+taskid+"] Filling cost of project "+pid+" with "+totalcost+"");
				$('tr#'+pid+' td span.costlabel').html(totalcost);
				$('tr#'+pid+' td span.costcurrency').show();
				}
			else if (totalcost==0) {
				$('tr#'+pid+' td span.costlabel').html("");
				$('tr#'+pid+' td span.costcurrency').hide();
				}
			
			//*******************************/
			// FILL NEXT STEP
			// Get task id
			taskid=$(this).attr('id');
			// Get project PID (from task line)
			var pid = $(this).attr( "pid" );
			// Task should be critical
			var iscritical = $(this).find("span.setcritical").hasClass("critical");
			// Get HTML content of nextstep cell
			var nextstep = $('tr#'+pid+' td.nextstep').html();
			// Get percentage of finished value
			percent=$(this).find('td.taskconfig.progress > div.progress-wrapper > div.value').attr('percent');
			// Task has not to be done
			var isdone = $(this).hasClass("done");
			//console.log("Task is done : "+isdone);
			// Fill nextstep if task is critical and that netstep is not already filled yet
			if (iscritical && nextstep=='' && ! isdone) {
				//console.log("Cette tâche est la prochaine tache critique du projet "+pid);
				//var manager = $(this).find("td.taskmanager").justtext();
				var manager = $(this).find("td.taskmanager .mlabel").html();
				//console.log("["+taskid+"] Remplissage de nextstep du projet "+pid+" avec "+manager+" (tâche terminée à "+percent+" %)");

				$('tr#'+pid+' td.nextstep').html(manager);
				// Task percentage
				if (percent > 0 && percent < 70) cssprct='ok';
				else if (percent > 70 && percent < 100) cssprct='warning';
				else if (percent==0 || percent >= 100) cssprct='error';
				else cssprct='';
				//
				if (percent!=undefined && percent > 0) {
					$('tr#'+pid+' td.nextstep').prop('title',"Tâche a "+percent+" % de sa deadline.");
					}
				else $('tr#'+pid+' td.nextstep').prop('title',"Pas de deadline sur cette tâche !");
				// Colorize
				$('tr#'+pid+' td.nextstep').removeClass('ok').removeClass('warning').removeClass('error').addClass(cssprct);
				}
				
			// Progress bar
			//$(this).$('td .progress-wrapper').each(function(i, obj) {
			finalvalue=$(this).find("td div.progress-wrapper div.value").attr('percent');
			if (finalvalue>100) {
				$(this).find("td.progress > div.progress-wrapper > div.bar-container > div.bar").css('background-color','#882723');
				$(this).find("td.progress > div.progress-wrapper > div.bar-container > div.bar").css('width','100%');
				$(this).find("td.progress > div.progress-wrapper > div.value").html(finalvalue+"%");
				//console.log("Task "+pid+ " is more than 100 : "+$(this).find("td.progress > div.progress-wrapper > div.bar-container").html());
				}
			else if (finalvalue==0) {
				$(this).find("td.progress > div.progress-wrapper > div.bar-container > div.bar").css('background-color','#eeeeee');
				$(this).find("td.progress > div.progress-wrapper > div.bar-container > div.bar").css('width','100%');
				$(this).find("td.progress > div.progress-wrapper > div.value").html("---- ? ----");
				}
			else if (finalvalue != undefined) {
				//console.log(finalvalue+'%');
				$(this).find("td.progress > div.progress-wrapper > div.bar-container > div.bar").css('width',finalvalue+'%');
				$(this).find("td.progress > div.progress-wrapper > div.value").html(finalvalue+"%");
				//$(this).childNodes[1].innerHTML = finalvalue  + "%";
				}

			});
			//}
	
		}


	function unifiedtask2(element=null) {
		if (element==null) element=$('.task');
		else element=$('.task[pid='+element+']');
		//if (element==null) element='.task';
		//else element='.task[pid='+element+']';

		var totalcost=0;
//		var element = $(element); //collect all spans as array
		
		//$(element).each(function(i, obj) {
                for (var i = 0, len = element.length; i < len; i++) { //for loop
                	let myid = element[i].id
                	// Continue for task with empty ID (new task line)
                	if (myid=="") continue;
                	console.log(myid);
			//*******************************/
			// DISPLAY TASK COUNT
			var taskcount = $("#"+myid).attr( "taskcount" );
			var pid = $("#"+myid).attr( "pid" );
			//console.log("Alimentation du nombre de tâches ("+taskcount+") du projet "+pid+".");
			$('tr#'+pid+' td.pname span.circled-wrapper span.circled').html(taskcount);
			if (taskcount>0) {$('tr#'+pid+' td.pname span.circled-wrapper').show();}
			
			
			
			//*******************************/
			// TOTAL COST OF PROJECT
			var taskcount = $("#"+myid).attr( "taskcount" );
			//console.log(taskcount);
			if (taskcount=="1") {
				totalcost=0;
				}
			// Get task id
			taskid=$("#"+myid).attr('id');
			// Get project PID (from task line)
			var pid = $("#"+myid).attr( "pid" );
			// Task should be critical
			// Get HTML content of nextstep cell
			var cost = $("#"+myid).find("td.taskcost > input.cost").val();
			cost=Number(cost);
			//console.log("COUT : "+cost);
			if (cost && typeof cost === "number" && cost != "") {
				totalcost += cost;
				console.log("COUT TOTAL : "+totalcost);
				// Fill cost
				console.log("["+taskid+"] Filling cost of project "+pid+" with "+totalcost+"");
				$('tr#'+pid+' td span.costlabel').html(totalcost);
				$('tr#'+pid+' td span.costcurrency').show();
				}
			else if (totalcost==0) {
				$('tr#'+pid+' td span.costlabel').html("");
				$('tr#'+pid+' td span.costcurrency').hide();
				}
			
			//*******************************/
			// FILL NEXT STEP
			// Get task id
			taskid=$("#"+myid).attr('id');
			// Get project PID (from task line)
			var pid = $("#"+myid).attr( "pid" );
			// Task should be critical
			var iscritical = $("#"+myid).find("td.actions > span.setcritical").hasClass("critical");
			// Get HTML content of nextstep cell
			var nextstep = $('tr#'+pid+' td.nextstep').html();
			// Get percentage of finished value
			percent=$("#"+myid).find('td.taskconfig.progress > div.progress-wrapper > div.value').attr('percent');
			// Task has not to be done
			var isdone = $("#"+myid).hasClass("done");
			//console.log("Task is done : "+isdone);
			// Fill nextstep if task is critical and that netstep is not already filled yet
			if (iscritical && nextstep=='' && ! isdone) {
				//console.log("Cette tâche est la prochaine tache critique du projet "+pid);
				//var manager = $(this).find("td.taskmanager").justtext();
				var manager = $("#"+myid).find("td.taskmanager > span.mlabel").html();
				//console.log("["+taskid+"] Remplissage de nextstep du projet "+pid+" avec "+manager+" (tâche terminée à "+percent+" %)");

				$('tr#'+pid+' td.nextstep').html(manager);
				// Task percentage
				if (percent > 0 && percent < 70) cssprct='ok';
				else if (percent > 70 && percent < 100) cssprct='warning';
				else if (percent==0 || percent >= 100) cssprct='error';
				else cssprct='';
				//
				if (percent!=undefined && percent > 0) {
					$('tr#'+pid+' td.nextstep').prop('title',"Tâche a "+percent+" % de sa deadline.");
					}
				else $('tr#'+pid+' td.nextstep').prop('title',"Pas de deadline sur cette tâche !");
				// Colorize
				$('tr#'+pid+' td.nextstep').removeClass('ok').removeClass('warning').removeClass('error').addClass(cssprct);
				}
				
			// Progress bar
			//$(this).$('td .progress-wrapper').each(function(i, obj) {
			finalvalue=$("#"+myid).find("td.progress > div.progress-wrapper > div.value").attr('percent');
			if (finalvalue>100) {
				$("#"+myid).find("td.progress > div.progress-wrapper > div.bar-container > div.bar").css('background-color','#882723');
				$("#"+myid).find("td.progress > div.progress-wrapper > div.bar-container > div.bar").css('width','100%');
				$("#"+myid).find("td.progress > div.progress-wrapper > div.value").html(finalvalue+"%");
				//console.log("Task "+pid+ " is more than 100 : "+$(this).find("td.progress > div.progress-wrapper > div.bar-container").html());
				}
			else if (finalvalue==0) {
				$("#"+myid).find("td.progress > div.progress-wrapper > div.bar-container > div.bar").css('background-color','#eeeeee');
				$("#"+myid).find("td.progress > div.progress-wrapper > div.bar-container > div.bar").css('width','100%');
				$("#"+myid).find("td.progress > div.progress-wrapper > div.value").html("---- ? ----");
				}
			else if (finalvalue != undefined) {
				//console.log(finalvalue+'%');
				$("#"+myid).find("td.progress > div.progress-wrapper > div.bar-container > div.bar").css('width',finalvalue+'%');
				$("#"+myid).find("td.progress > div.progress-wrapper > div.value").html(finalvalue+"%");
				//$(this).childNodes[1].innerHTML = finalvalue  + "%";
				}

			//});
			}
	
		}

	/***********************************/
	/****** UNIFIED JS FOR EACH TASK ***/
	/***********************************/

	function unifiedproject(element=null) {
		//if (element==null) element='.project';
		//else element='.project[id='+element+']';
		if (element==null) element=$('.project');
		else element=$('.project[id='+element+']');
		
                for (var i = 0, len = element.length; i < len; i++) { //for loop
                	let myid = element[i].id
			//$(element).each(function(i, obj) {
			// Progress bar
			//$(this).$('td .progress-wrapper').each(function(i, obj) {
			finalvalue=$("#"+myid).find("td.projectconfig div.progress-wrapper div.value").attr('percent');
			//console.log(finalvalue);
			if (finalvalue>100) {
				$("#"+myid).find("td.projectconfig > div.progress-wrapper > div.bar-container > div.bar").css('background-color','#882723');
				$("#"+myid).find("td.projectconfig > div.progress-wrapper > div.bar-container > div.bar").css('width','100%');
				$("#"+myid).find("td.projectconfig > div.progress-wrapper > div.value").html(finalvalue+"%");
				//console.log("Task "+pid+ " is more than 100 : "+$(this).find("td.progress > div.progress-wrapper > div.bar-container").html());
				}
			else if (finalvalue==0) {
				$("#"+myid).find("td.projectconfig > div.progress-wrapper > div.bar-container > div.bar").css('background-color','#eeeeee');
				$("#"+myid).find("td.projectconfig > div.progress-wrapper > div.bar-container > div.bar").css('width','100%');
				$("#"+myid).find("td.projectconfig > div.progress-wrapper > div.value").html("---- ? ----");
				}
			else if (finalvalue != undefined) {
				//console.log(finalvalue+'%');
				$("#"+myid).find("td.projectconfig > div.progress-wrapper > div.bar-container > div.bar").css('width',finalvalue+'%');
				$("#"+myid).find("td.projectconfig > div.progress-wrapper > div.value").html(finalvalue+"%");
				//$(this).childNodes[1].innerHTML = finalvalue  + "%";
				}

			//});
			}
		}


	/***********************/
	/****** PROGRESS BAR - UNUSED ANYMORE - COULD BE DELETED***/
	/***********************/
	function progressbar2() {
		//console.log("Passage dans progressbar2");
		//var elem=document.querySelectorAll('.bar');
		$('.progress-wrapper').each(function(i, obj) {
			finalvalue=$(this).children(".value").attr('percent');
			//console.log("Coucou de la valeur "+finalvalue);
			//console.log("Final Value : "+finalvalue+".Fin de value")
			if (finalvalue>100) {
				$(this).find(".bar-container .bar").css('background-color','#882723');
				$(this).find(".bar-container .bar").css('width','100%');
				$(this).children(".value").html(finalvalue+"%");
				}
			else if (finalvalue==0) {
				$(this).find(".bar-container .bar").css('background-color','#eeeeee');
				$(this).find(".bar-container .bar").css('width','100%');
				$(this).children(".value").html("---- ? ----");
				}
			else if (finalvalue != undefined) {
				//console.log(finalvalue+'%');
				$(this).find(".bar-container .bar").css('width',finalvalue+'%');
				$(this).children(".value").html(finalvalue+"%");
				//$(this).childNodes[1].innerHTML = finalvalue  + "%";
				}
			});
		}
	/*
	function progressbar() {
		//var elem=document.querySelectorAll('.bar');
		for (let e of document.getElementsByClassName("bar")) {
			//console.log("Coucou !"+e.innerText);
			finalvalue=Number(e.innerText);
			if (finalvalue>100) {
				e.style.backgroundColor = "red";
				e.style.width = "100%";
				e.innerHTML = finalvalue  + "%";
				}
			else {
				e.style.width = finalvalue + "%";
				e.innerHTML = finalvalue  + "%";
				}
			}
		}
	*/
	function ScrollTo() {
		//console.log(get);
		if (get['pid'] && get['pid']!=''){
			pid=get['pid'];
			//$(document.body).delay(3000).animate({scrollTop: $('tr[project='+pid+']').offset().top }, 3000);
			document.getElementById(pid).scrollIntoView({block: 'start', behavior: 'smooth'});
			ShowTasks(pid);
			}
	}

	// Full refresh of page and eventually scrool to a given project id
	function RefreshPage(pid=false,setvalue=false) {
		// If a pid is given, pass it to URL
		if (pid != false) {argurl="?pid="+pid}
		else {argurl="";}
		window.location.href = getPathFromUrl(window.location.href) + argurl;
	}

	// Refresh project with a given pid
	function RefreshProject(pid,pfile,pcount) {
		if (! pid || ! pfile || ! pcount) {console.log("Il manque un element pour rafraichir (pid:"+pid+", pfile:"+pfile+",pcount:"+pcount+")");return false;}
		console.log("Refreshing project pid "+pid+" with pcount "+pcount+" and pfile "+pfile+"");
		$.ajax({
			url: 'include/ajax.php',
			type: 'POST',
			data: {
				f : pfile,
				action: 'refresh',
				pcount : pcount
				},
			error: function(data) {
				console.log('Error refreshing project '+pid);
				console.log(data);
				},
			success: function(data) {
				//console.log(data);
				$('tr.task').hide();
				console.log("Deleting all tasks having pid "+pid+"");
				$("tr[pid="+pid+"]").remove();
				console.log("Refreshing project content of project with id "+pid+".");
				$("tr#"+pid).replaceWith(data);
				// Show tasks again
				ShowTasks(pid);
				// Refresh NextStep for this project id
				//fillnextstep(pid);
				// Refresh project Cost
				//fillprojectcost(pid);
				unifiedtask2(pid);
				unifiedproject(pid);
				// Refresh progess bars
				//progressbar2();
				} // Ajax success
			});// Ajax call
	}

	/****************************************/
	/******** ACTION BUTTONS ****************/
	/****************************************/

	// Set project done
	$(document.body).on('click', 'tr.project td span.setdone' ,function(){
		oclass='done';
		if ($(this).hasClass(oclass)==false) {var setvalue=1;} else {var setvalue=0;}
		var po = $(this).closest("tr.project");
		var pid = po.attr( "id" );
		var file = po.attr( "pfile" );
		var button = $(this);
		console.log("Going to modify file "+file+" with value "+oclass+"="+setvalue+"");
		$.ajax({
				url: 'include/ajax.php',
				type: 'POST',
				data: {
					f : file,
					config : oclass,
					value : setvalue
					},
				error: function(data) {
					console.log('Error switching value '+oclass+'');
					console.log(data);
					},
				success: function(data) {
					console.log('Congrats ;) Project is finished ! Refreshing !');
					RefreshPage(pid);
					}
			});// Ajax call
		});

	// Archive project
	$(document.body).on('click', 'tr.project td span.archive' ,function(){
		config='archive';
		var po = $(this).closest("tr.project");
		var pid = po.attr( "id" );
		var file = po.attr( "pfile" );
		var button = $(this);
		console.log("Going to archive directory containing "+file+" .");
		$.ajax({
				url: 'include/ajax.php',
				type: 'POST',
				data: {
					f : file,
					action : 'archive'
					},
				error: function(data) {
					console.log('Error archiving project '+oclass+'');
					console.log(data);
					},
				success: function(data) {
					console.log('Congrats ;) Project has been moved to archive ! Refreshing !');
					console.log(data);
					RefreshPage(pid);
					}
			});// Ajax call
		});



	// Set task done
	$(document.body).on('click', 'tr.task td span.setdone' ,function(){
		oclass='done';
		if ($(this).hasClass(oclass)==false) {var setvalue=1;} else {var setvalue=0;}
		var po = $(this).closest("tr.project");
		var to = $(this).closest("tr.task");
		var project = po.attr( "project" );
		//var projectname = $(this).parent().attr( "projectname" );
		var file = to.attr( "tfile" );
		var taskid = to.attr( "id" );
		var button = $(this);
		console.log("Going to modify file "+file+" (tak id "+taskid+")  with value "+oclass+"="+setvalue+"");

		$.ajax({
				url: 'include/ajax.php',
				type: 'POST',
				data: {
					f : file,
					config : oclass,
					value : setvalue
					},
				error: function(data) {
					console.log('Error switching value '+oclass+'');
					console.log(data);
					},
				success: function(data) {
					if ($('tr#'+taskid+' td.firsttaskconfig').hasClass('noini')) {
						console.log("Info : cas numero 1.");
						$('tr#'+taskid).children("td.firsttaskconfig")
							.removeClass('noini')
							.addClass('green1')
							.html('Tâche remise à l\'état -en cours- !');
						}
					else {
						console.log("Info : The task "+taskid+" originaly already contain an ini file.");
						$('tr#'+taskid+' td.firsttaskconfig')
							.attr('colspan',6)
							.attr('class','')
							.addClass('firsttaskconfig green1');
						if (setvalue==1) $('tr#'+taskid+' td.firsttaskconfig').html('Tâche terminée !');
						else	$('tr#'+taskid+' td.firsttaskconfig').html('Tâche remise à l\'état -en cours- !');
						$('tr#'+taskid+' td.taskconfig').remove();
						}
					button.toggleClass(oclass);
					} // Ajax success

			});// Ajax call
		});

	// Display extra tools
	$(document.body).on('click', '.toolsbutton', function(){
		console.log("Displaying extra tools :)");
		let item = $(this).nextAll('div.tools');
		item.toggle();
		
	});

	// Send BOOST email
	$(document.body).on('click', '.taskemail', function() {
		var to = $(this).closest("tr.task");
		var po = to.prevAll("tr.project").first();
		var project = po.attr( "project" );
		var task = to.find("td.taskname span.tasknamevalue").html();
		var manager = to.find("td.taskmanager input.manager").val();
		if (manager=='') {
			console.log('No manager to send an email to.');
			return false;
			}
		var file = to.attr( "tfile" );
		var deadlinestring = to.find("td.deadline .discret1").html();
		//console.log("DeadlineString : "+deadlinestring);
		sure=window.confirm("Êtes-vous sûr(e) de vouloir relancer "+manager+" ?");
		//console.log(sure);
		if ( ! sure) {
			console.log("You've canceled email action");
			return false;
			}
		if (deadlinestring!=undefined) {
			var deadlinedate = to.find("td.deadline input").val();
			}
		console.log("Envoi d'un mail à "+manager+" pour la tache "+task+" du projet "+project+" expirant le "+deadlinestring+" ("+deadlinedate+").")
		$.ajax({
				url: 'include/ajax.php',
				type: 'POST',
				data: {
					taskemail : true,
					p : project,
					t : task,
					f : file,
					manager : manager,
					deadline : deadlinedate,
					deadlinestring : deadlinestring
					},
				error: function(data) {
					alert('Error refreshing data.');
					console.log(data);
					},
				success: function(data) {
					console.log(data);
					//$('#resultemailtestajax').html(data);
					} // Ajax success

			});// Ajax call
		});


	// Send email (task manager notification)
	$(document.body).on('click', '.noticemanager', function() {
		var to = $(this).closest("tr.task");
		var po = to.prevAll("tr.project").first();
		var project = po.attr( "project" );
		var task = to.find("td.taskname span.tasknamevalue").html();
		var manager = to.find("td.taskmanager input.manager").val();
		if (manager=='') {
			console.log('No manager to send an email to.');
			return false;
			}
		var file = to.attr( "tfile" );
		var deadlinestring = to.find("td.deadline .discret1").html();
		//console.log("DeadlineString : "+deadlinestring);
		sure=window.confirm("Êtes-vous sûr(e) de vouloir informer "+manager+" qu'il est responsable de cette tâche ?");
		//console.log(sure);
		if ( ! sure) {
			console.log("You've canceled email action");
			return false;
			}
		if (deadlinestring!=undefined) {
			var deadlinedate = to.find("td.deadline input").val();
			}
		console.log("Envoi d'un mail à "+manager+" pour la tache "+task+" du projet "+project+" expirant le "+deadlinestring+" ("+deadlinedate+").")
		$.ajax({
				url: 'include/ajax.php',
				type: 'POST',
				data: {
					noticemanager : true,
					p : project,
					t : task,
					f : file,
					manager : manager,
					deadline : deadlinedate,
					deadlinestring : deadlinestring
					},
				error: function(data) {
					alert('Error refreshing data.');
					console.log(data);
					},
				success: function(data) {
					console.log(data);
					//$('#resultemailtestajax').html(data);
					} // Ajax success

			});// Ajax call
		});

	function ChangeDeadline(id,file,start,deadline) {
		console.log('Changing deadline on :'+id+" which is starting on : "+start+". Working on file "+file+".");
		console.log('We will set deadline as :'+deadline);
		$.ajax({
				url: 'include/ajax.php',
				type: 'POST',
				data: {
					f : file,
					config : 'deadline',
					start : start,
					value : deadline
					},
				error: function(data) {
					console.log("Error changing deadline : "+data);
					},
				success: function(data) {
					console.log("Successfully changed deadline : "+data);
					//$('#resultemailtestajax').html(data);
					} // Ajax success

			});// Ajax call
	}

	function ChangeDate(id,file,element,newdate) {
		$.ajax({
			url: 'include/ajax.php',
			type: 'POST',
			data: {
				f : file,
				config : element,
				value : newdate
				},
			error: function(data) {
				console.log("Error changing element "+element+" with new date : "+data);
				},
			success: function(data) {
				console.log("Successfully changed element "+element+" with new date : "+data);
				//$('#resultemailtestajax').html(data);
				} // Ajax success

			});// Ajax call
		}

	// Change date multi selector
	$(document.body).on('change', 'input.datechangerdate,select.datechangeroption' ,function(){
		if ($(this).closest("tr").hasClass('project')) {
			var type="project";
			//var to = $(this).closest("tr.task");
			var po = $(this).closest("tr.project");
			var file = po.attr( "pfile" );
			}
		else if ($(this).closest("tr").hasClass('task')) {
			var type="task";
			var to = $(this).closest("tr.task");
			var po = to.prevAll("tr.project").first();
			var file = to.attr( "tfile" );
			}

		// Get project info to refresh at the end
		var pid = po.attr( "id" );
		var pfile = po.attr( "pfile" );// Required for refresh
		var pcount = po.attr( "pcount" );

		// Get td id
		var id=$(this).closest("td").attr('id');

		// Get date from td childs
		newdate=$("#"+id).find("input.datechangerdate").val();
		element=$("#"+id).find("select.datechangeroption").val();

		// exit if element empty
		if (element=="" || newdate=="" || newdate==undefined) { console.log("One the required elements is empty, nothing done. (newdate="+newdate+", element="+element+", id="+id+")"); return false;}

		// Get start date and element id
		var id = $(this).parents("tr").attr( "id" );
		var start = $(this).parents("tr").attr( "start" );

		console.log("Setting date "+newdate+" on element "+element+" for file "+file);

		if (element=='deadline') {
			ChangeDeadline(id,file,start,newdate);
			}
		else {ChangeDate(id,file,element,newdate);}
		// Refresh
		RefreshProject(pid,pfile,pcount);
		});

	// Set task critical
	$(document.body).on('click', 'span.setcritical' ,function(){
		oclass='critical';
		if ($(this).hasClass(oclass)==false) {var setvalue=1;} else {var setvalue=0;}
		//var po = $(this).closest("tr.project");
		var to = $(this).closest("tr.task");
		//var project = po.attr( "project" );
		var file = to.attr( "tfile" );
		//var projectname = to.attr( "projectname" );
		console.log("Going to modify task file "+file+" with value "+oclass+"="+setvalue+"");
		var button = $(this);
		$.ajax({
				url: 'include/ajax.php',
				type: 'POST',
				data: {
					f : file,
					config : oclass,
					value : setvalue
					},
				error: function(data) {
					console.log('Error switching value '+oclass+'');
					console.log(data);
					},
				success: function(data) {
					console.log(data);
					console.log('On toggle la classe '+oclass+' sur le bouton');
					button.toggleClass(oclass);
					} // Ajax success

			});// Ajax call
		});

	// Set Project DPI
	$(document.body).on('click', 'span.setdpi' ,function(){
		
		oclass='dpi';
		if ($(this).hasClass(oclass)==false) {var setvalue=1;} else {var setvalue=0;}
		var po = $(this).closest("tr.project");
		var project = po.attr( "project" );
		var pfile = po.attr( "pfile" );
		//var projectname = po.attr( "projectname" );
		console.log("Going to modify file "+pfile+" with value "+oclass+"="+setvalue+"");
		var button = $(this);
		$.ajax({
				url: 'include/ajax.php',
				type: 'POST',
				data: {
					f : pfile,
					config : oclass,
					value : setvalue
					},
				error: function(data) {
					console.log('Error switching value '+oclass+'');
					console.log(data);
					},
				success: function(data) {
					console.log(data);
					console.log('On toggle la classe '+oclass+' sur le bouton');
					button.toggleClass(oclass);
					} // Ajax success

			});// Ajax call
		});
	// Set project MASTERPLAN
	$(document.body).on('click', 'span.setmasterplan' ,function(){
		oclass='masterplan';
		if ($(this).hasClass(oclass)==false) {var setvalue=1;} else {var setvalue=0;}
		var po = $(this).closest("tr.project");
		var project = po.attr( "project" );
		var pfile = po.attr( "pfile" );
		//var projectname = po.attr( "projectname" );
		console.log("Going to modify file "+pfile+" with value "+oclass+"="+setvalue+"");
		var button = $(this);
		$.ajax({
				url: 'include/ajax.php',
				type: 'POST',
				data: {
					f : pfile,
					config : oclass,
					value : setvalue
					},
				error: function(data) {
					console.log('Error switching value '+oclass+'');
					console.log(data);
					},
				success: function(data) {
					console.log(data);
					console.log('On toggle la classe '+oclass+' sur le bouton');
					button.toggleClass(oclass);
					} // Ajax success

			});// Ajax call
		});

	// Refresh TASK
	$(document.body).on('click', 'span.refresh' ,function(){
		var po = $(this).closest("tr.project");
		var pid = po.attr( "id" );
		var pfile = po.attr( "pfile" );
		var pcount = po.attr( "pcount" );
		RefreshProject(pid,pfile,pcount);
		//$("tr#"+pid).replaceWith(data);
		});

	/****************************************/
	/******** TASK AND PROJECT FIELDS *******/
	/****************************************/

	// Display Tasks on click on Project name
	$(document.body).on('click', 'tr.project td.pname' ,function(e){
		if ($(e.target).is('span.commentnotification, input.commentinput, span.projectnameicon')){
			e.stopPropagation();
			return;
			}
		var pid = $(this).parent().attr( "id" );
		console.log("Clicking on project with id "+pid);
		// Then toggle clicked one
		if ($('tr.task[pid='+pid+']').not('.hide').is(":hidden")) {
			// First hide any other open project
			$('tr.task').hide();
			// Show up
			//$('tr.task[pid='+pid+']').show("slow");
			ShowTasks(pid);
			}
		else {
			console.log("Hidding all task of pid "+pid);
			$('tr.task[pid='+pid+']').hide();
			}
		});


	// Toggle temporarily task done display - toggletaskdone
	$(document.body).on('click', 'span.toggletaskdone' ,function(e){
		var po = $(this).closest("tr.project");
		var pid = po.attr( "id" );
		console.log("Clocked on toggle button for project "+pid);
		if ($('tr.task[pid='+pid+']').hasClass('hide')) {
			console.log("Showing hidden tasks of project "+pid);
			$('tr.task[pid='+pid+'].hide').show();
			$('tr.task[pid='+pid+'].hide').removeClass('hide');
			}
		else {
			console.log("Hiding done tasks of project "+pid);
			$('tr.task[pid='+pid+'].done').hide();
			$('tr.task[pid='+pid+'].done').addClass('hide');
			}
		});
	

	// On change on a datepicker field
	$(document.body).on('change', 'input.deadlinepicker' ,function(){
		deadline=this.value;
		if ($(this).parent().parent().hasClass('project')) {
			var file = $(this).parent().parent().attr( "pfile" );
			}
		else if ($(this).parent().parent().hasClass('task')) {
			var file = $(this).parent().parent().attr( "tfile" );
			}
		var id = $(this).parent().parent().attr( "id" );
		var start = $(this).parent().parent().attr( "start" );
		ChangeDeadline(id,file,start,deadline);
	});

	/*
	// Autocomplete fields
	$(document.body).on('focus', 'input.autocompletefield' ,function(){
		//var fieldname = $(this).attr( "fieldname" );
		$(this).autocomplete({
			source: function( request, response ) {
			//console.log(request.term)
			$.ajax({
				url : 'include/ajax.php',
				dataType: "jsonp",
				data: {
					autocomplete:true,
					SearchTerm: request.term,
					json: true
				},
			success: function( data ) {
				if (data!=null) {
					console.log(data);
					response(data);
					}
				},
			error: function(data) {
				console.log("Error autocompleting field "+field+" : " + data);
				},
			type: 'POST'
			});
		},
		autoFocus: true,
		minLength: 1
		});
	});
	*/


	// Display comment on click on Project info span
	$(document.body).on('click', 'tr.project td.pname span.commentnotification' ,function(e){
		e.stopPropagation();
		var pid = $(this).parent().parent().attr( "id" );
		console.log("Clicking on comment of project with id "+pid);
		let item = $(this).nextAll('input.commentinput');
		item.toggle();
	});

	// Display task name changer on click on task name
	$(document.body).on('click', 'tr.task td.taskname span.tasknamevalue' ,function(e){
		e.stopPropagation();
		console.log("Displaying task name changer");
		let item = $(this).nextAll('input.tasknameinput').first();
		item.toggle();
	});

	// Display project name changer on click on project button
	$(document.body).on('click', 'tr.project td.pname span.projectnameicon' ,function(e){
		e.stopPropagation();
		console.log("Displaying project name changer");
		let item = $(this).nextAll('input.projectnameinput').first();
		item.toggle();
	});

	// Display comment changer on click on bullet
	$(document.body).on('click', 'tr.task td span.commentbullet' ,function(e){
		e.stopPropagation();
		console.log("Displaying comment changer");
		let item = $(this).nextAll('input.commentinput').first();
		item.toggle();
	});

	// Display manager on click on manager name
	$(document.body).on('click', '.taskmanager, .projectmanager', function(e){
		//console.log($(e.target));
		let item = $(this).children('input.manager');
		if (item.css('display') === 'none') {
			item.show();
			item.css('display', 'block');
		} else if (item.css('display') === 'block') {
			if ($(e.target).is('input.manager')){
				 e.stopPropagation();
				 return;
			    }
			item.hide();
			item.css('display', 'none');
		}
	});

	// Display team on click on team name
	$(document.body).on('click', '.taskteam, .projectteam', function(e){
		//console.log($(e.target));
		let item = $(this).children('input.team');
		if (item.css('display') === 'none') {
			item.show();
			item.css('display', 'block');
		} else if (item.css('display') === 'block') {
			if ($(e.target).is('input.team')){
				 e.stopPropagation();
				 return;
			    }
			item.hide();
			item.css('display', 'none');
		}
		});

	// Display cost on click on cost
	$(document.body).on('click', '.taskcost', function(e){
		//console.log($(e.target));
		let item = $(this).children('input.cost');
		if (item.css('display') === 'none') {
			item.show();
			item.css('display', 'block');
		} else if (item.css('display') === 'block') {
			if ($(e.target).is('input.cost')){
				 e.stopPropagation();
				 return;
			    }
			item.hide();
			item.css('display', 'none');
		}
		});


	// Change project/task priority
	$(document.body).on('change', 'select.ppriority,select.tpriority' ,function(){
		var type='project';
		// Recup de la valeur de la nouvelle prio
		var setvalue=$(this).val();
		// On choppe l'identifiant du projet
		var pid=$(this).parent().parent().attr( "pid" );
		if (! pid) {var pid=$(this).parent().parent().attr( "id" );}
		//var pname=$(this).parent().parent().attr( "project" );
		// On choppe le nom de fichier de l'élément qu'on modifie
		var file = $(this).parent().parent().attr( "pfile" );
		// Si on ne modifie pas un projet alors c'est surement une tâche
		if (! file) {
			// Eventually make changes in this file
			file = $(this).parent().parent().attr( "tfile" );
			type='task';
			// Get pfile to refresh the whole projet
			pfile=$('tr#'+pid).attr( "pfile" );
			pcount=$('tr#'+pid).attr( "pcount" );
			}
		//var name = $(this).parent().attr( "projectname" );
		//if (! name) {$(this).parent().attr( "taskname" );}
		console.log("Going to set priority on file "+file+" (type="+type+", pid="+pid+") with value "+setvalue+"");
		var button = $(this);
		$.ajax({
				url: 'include/ajax.php',
				type: 'POST',
				data: {
					f : file,
					config : 'priority',
					pid : pid,
					type: type,
					value : setvalue
					},
				error: function(data) {
					console.log('Error changing priority to value '+setvalue+'');
					console.log(data);
					},
				success: function(data) {
					console.log("Type d'element modifié : "+type);
					// If it a task, replace tr content by new one
					if (type=='task') {
						RefreshProject(pid,pfile,pcount);
						}
					else if (type='project') {
						var spid=pid.split('-');
						spid[0]=setvalue;
						var newpid=spid.join('-');
						RefreshPage(newpid);
						}
					else {
						button.parent().parent().hide(300);
						}
					} // Ajax success

			});// Ajax call
	});

	// Change Manager
	$(document.body).on('change', 'input.manager' ,function(){
		config='manager';
		if ($(this).parent().parent().hasClass('project')) {
			//var id = $(this).parent().parent().attr( "project" );
			var file = $(this).parent().parent().attr( "pfile" );
			}
		else if ($(this).parent().parent().hasClass('task')) {
			//var id = $(this).parent().parent().attr( "taskid" );
			var file = $(this).parent().parent().attr( "tfile" );
			}
		value=$(this).val();
		console.log("Going to modify file "+file+" with value "+config+"="+value+"");
		var button = $(this);
		$.ajax({
				url: 'include/ajax.php',
				type: 'POST',
				data: {
					f : file,
					config : config,
					value : value
					},
				error: function(data) {
					console.log('Error setting value');
					console.log(data);
					},
				success: function(data) {
					console.log(data);
					console.log('Changing manager OK');
					} // Ajax success

			});// Ajax call
		htmlvalue=value.replace(/,/g,'<br />');
		$(this).prev().html(htmlvalue);
		$(this).hide();
	});


	// Change Comment
	$(document.body).on('change', 'input.commentinput' ,function(){
		config='comment';
		if ($(this).parent().parent().hasClass('project')) {
			var file = $(this).parent().parent().attr( "pfile" );
			type="project";
			}
		else if ($(this).parent().parent().hasClass('task')) {
			var file = $(this).parent().parent().attr( "tfile" );
			type="task";
			}
		var value=$(this).val();
		value = value.replace(/[^\w\s]/gi, '');
		console.log("Going to modify file "+file+" with value "+config+"="+value+"");
		var button = $(this);
		$.ajax({
				url: 'include/ajax.php',
				type: 'POST',
				data: {
					f : file,
					config : config,
					value : value
					},
				error: function(data) {
					console.log('Error setting value');
					console.log(data);
					},
				success: function(data) {
					console.log(data);
					console.log('Changing comment OK');
					if (type=="project") {
						button.prev("span.commentnotification").prop('title',value);
						if (value!="") {button.prev("span.commentnotification").addClass("active");}
						else if (value=="") {button.prev("span.commentnotification").removeClass("active");}
						}
					else if (type=="task") {
						button.parent().prop('title',value);
						if (value!="") {button.prev("span.circled").addClass('comment');}
						else if (value=="") {button.prev("span.circled").removeClass('comment');}
						}
					} // Ajax success

			});// Ajax call
		//htmlvalue=value.replace(/,/g,'<br />');
		//$(this).prev().html(htmlvalue);
		$(this).hide();
	});



	// Rename
	$(document.body).on('change', 'input.tasknameinput,input.projectnameinput' ,function(){
		//var to = $(this).closest("tr.task");
		//var po = $(this).prevAll("tr").first();
		if ($(this).closest("tr").hasClass('project')) {
			var file = $(this).closest("tr").attr( "pfile" );
			var type="project";
			}
		else if ($(this).closest("tr").hasClass('task')) {
			var to = $(this).closest("tr.task");
			var po = to.prevAll("tr.project").first();
			var file = to.attr( "tfile" );
			var pid=po.attr("id");
			var pfile=po.attr("pfile");
			var pcount=po.attr( "pcount" );
			console.log(pid);
			var type="task";
			}
		console.log(file);
		//return;
		var value=$(this).val();
		//value = value.replace(/[^\w\s]/gi, '');
		console.log("Going to rename element with file "+file+" .");
		var button = $(this);
		$.ajax({
				url: 'include/ajax.php',
				type: 'POST',
				data: {
					f : file,
					config : 'rename',
					value : value
					},
				error: function(data) {
					console.log('Error renaming element.');
					console.log(data);
					},
				success: function(data) {
					console.log(data);
					console.log('Renaming OK');
					if (type=="project") {
						console.log(data);
						//button.prev("span.commentnotification").prop('title',value);
						//if (value!="") {button.prev("span.commentnotification").addClass("active");}
						//else if (value=="") {button.prev("span.commentnotification").removeClass("active");}
						RefreshPage();
						}
					else if (type=="task") {
						//button.parent().prop('title',value);
						//if (value!="") {button.prev("span.circled").addClass('comment');}
						//else if (value=="") {button.prev("span.circled").removeClass('comment');}
						console.log(data);
						RefreshProject(pid,pfile,pcount);
						}
					} // Ajax success

			});// Ajax call
		$(this).hide();
	});


	// Change Team
	$(document.body).on('change', 'input.team' ,function(){
		config='team';
		if ($(this).parent().parent().hasClass('project')) {
			var id = $(this).parent().parent().attr( "project" );
			var file = $(this).parent().parent().attr( "pfile" );
			}
		else if ($(this).parent().parent().hasClass('task')) {
			//var id = $(this).parent().parent().attr( "taskid" );
			var file = $(this).parent().parent().attr( "tfile" );
			}
		value=$(this).val();
		console.log("Going to modify file "+file+" with value "+config+"="+value+"");
		var button = $(this);
		$.ajax({
				url: 'include/ajax.php',
				type: 'POST',
				data: {
					f : file,
					config : config,
					value : value
					},
				error: function(data) {
					console.log('Error setting value');
					console.log(data);
					},
				success: function(data) {
					console.log(data);
					console.log('Changing team OK');
					} // Ajax success

			});// Ajax call
		htmlvalue=value.replace(/,/g,'<br />');
		$(this).prev().html(htmlvalue);
		$(this).hide();
	});


	// Change Cost
	$(document.body).on('change', 'input.cost' ,function(){
		config='cost';
		if ($(this).parent().parent().hasClass('project')) {
			var pid = $(this).parent().parent().attr( "id" );
			var id = $(this).parent().parent().attr( "project" );
			var file = $(this).parent().parent().attr( "pfile" );
			}
		else if ($(this).parent().parent().hasClass('task')) {
			var pid = $(this).parent().parent().attr( "pid" );
			//var id = $(this).parent().parent().attr( "taskid" );
			var file = $(this).parent().parent().attr( "tfile" );
			}
		value=$(this).val();
		console.log("Going to modify file "+file+" with value "+config+"="+value+"");
		var button = $(this);
		$.ajax({
				url: 'include/ajax.php',
				type: 'POST',
				data: {
					f : file,
					config : config,
					value : value
					},
				error: function(data) {
					console.log('Error setting value');
					console.log(data);
					},
				success: function(data) {
					console.log(data);
					console.log('Changing cost OK');
					} // Ajax success

			});// Ajax call
		htmlvalue=value.replace(/,/g,'<br />');
		// Set value input span
		$(this).prev().prev().html(htmlvalue);
		// Enable currency
		if (value=="") {$(this).prev().hide();}
		else {$(this).prev().show();}
		// Refresh project Cost
		fillprojectcost(pid);
		// Hide form
		$(this).hide();
	});

	/****************************************/
	/**NEW PROJECT AND NEW TASK FIELDS ******/
	/****************************************/

	// CREATE A PROJECT
	$(document.body).on('click', 'input#createproject' ,function(){
		var project = $(this).parent().parent().find("input#newproject_name").val();
		var priority = $(this).parent().parent().find("select#newproject_priority").val();
		console.log("Going to try to create project "+project+" with priority "+priority+"");
		var button = $(this);
		if (project!="" && priority!="") {
			$.ajax({
				url: 'include/ajax.php',
				type: 'POST',
				data: {
					action : 'createproject',
					priority:priority,
					project: project 
					},
				error: function(data) {
					console.log('Error creating project '+project+'');
					console.log(data);
					button.val(data);
					},
				success: function(data) {
					console.log('Project creation '+project+' in progress...');
					console.log(data);
					button.val(data);
					RefreshPage();
					} // Ajax success
				});// Ajax call
			}
		else
			{button.val("Formulaire incomplet !");}
		});


	// CREATE A PROJECT FROM TEMPLATE
	$(document.body).on('click', 'input#createprojecttemplate' ,function(){
		var project = $(this).parent().parent().find("input#newproject_name").val();
		var priority = $(this).parent().parent().find("select#newproject_priority").val();
		console.log("Going to try to create project "+project+" with priority "+priority+" from template.");
		var button = $(this);
		if (project!="" && priority!="") {
			$.ajax({
				url: 'include/ajax.php',
				type: 'POST',
				data: {
					action : 'createprojecttemplate',
					priority:priority,
					project: project 
					},
				error: function(data) {
					console.log('Error creating project '+project+' from template');
					console.log(data);
					button.val(data);
					},
				success: function(data) {
					console.log('Project creation '+project+' in progress...');
					console.log(data);
					button.val(data);
					RefreshPage();
					} // Ajax success
				});// Ajax call
			}
		else
			{button.val("Formulaire incomplet !");}
		});


	// CREATE A TASK
	$(document.body).on('click', 'input.createtask' ,function(){
		var pid = $(this).parent().parent().attr("pid");
		var taskname = $(this).parent().parent().find("input#newtask_name").val();
		var priority = $(this).parent().parent().find("select#newtask_priority").val();
		// Get pfile & pcount to refresh the whole projet
		var pfile=$('tr#'+pid).attr( "pfile" );
		var pcount=$('tr#'+pid).attr( "pcount" );
		var pdirname = $('tr#'+pid).attr("dirname");
		console.log("Going to try to create task "+taskname+" with priority "+priority+", in project pdirname "+pdirname+"");
		var button = $(this);
		if (taskname!="" && pdirname!="" && priority!="") {
			$.ajax({
				url: 'include/ajax.php',
				type: 'POST',
				data: {
					action : 'createtask',
					pdirname : pdirname,
					priority:priority,
					task: taskname 
					},
				error: function(data) {
					console.log('Error creating task '+taskname+'');
					console.log(data);
					button.val(data);
					},
				success: function(data) {
					console.log('Task creation '+taskname+' in progress...');
					console.log(data);
					button.val(data);
					RefreshProject(pid,pfile,pcount);
					} // Ajax success
				});// Ajax call
			}
		else
			{button.val("Formulaire incomplet !");}
		});

	/****************************************/
	/** AUTOCOMPLETE  ***********************/
	/****************************************/
	$(document.body).on('input', 'input.autocomplete' ,function(){
		// Get input base value
		var searchstr = $(this).val();
		//console.log(searchstr);
		// If it contains a comma, split
		if (searchstr.search(",")) {
			searcharr = $(this).val().split(",");
			// keep only last value
			searchterm = searcharr.pop();
			}
		// or keep full value of input field
		else {
			searchterm=searchstr;
			}
		//console.log(searchterm);
		//var field = $(this);
		$(this).autocomplete({
			source: function( request, response ) {
			//console.log(request.term)
				$.ajax({
					url : 'include/ajax.php',
					dataType: "jsonp",
					//type: 'POST',
					data: {
						action : 'adressbook',
						autocomplete:true,
						SearchTerm: searchterm,
						json: true
						},
				success: function( data ) {
					if (data!=null) {
						console.log("Autocomplete OK with search term "+searchterm+" : " + data);
						response(data);
						}
					},
				error: function(data) {
					console.log("Error autocompleting field : " + data);
					},
				type: 'POST'
				});
			},
			// The select will append selected item to the actual input value
			select: function( event, ui ) {
				//console.log( "Selected: " + ui.item.value + " aka " + ui.item.id );
				console.log( "Selected: " + ui.item.value);
				//field.val(searchstr+","+ui.item.value);
				//console.log("We set "+searcharr+","+ui.item.value);
				if (searcharr!="") {
					$(this).val(searcharr+","+ui.item.value);
					}
				else {
					$(this).val(ui.item.value);
					}	
				return false;
				},
			autoFocus: true,
			minLength: 1
		});
	});



	//**********************
	//******** Loading...
	//**********************
	/*
	$(document).ajaxSend(function(event, request, settings) {
	    $('#loading-indicator').show();
	});
	
	$(document).ajaxComplete(function(event, request, settings) {
	    $('#loading-indicator').hide();
	});
	*/

	//window.onload=filltaskcount();
	//window.onload=fillprojectcost();
	//window.onload=fillnextstep();
	window.onload=unifiedtask2();
	window.onload=unifiedproject();
	window.onload=ScrollTo();
        //window.onload=progressbar2();


});

