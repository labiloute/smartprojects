<?php
// VARIOUS SETTINGS
$_CONFIG['app-name']='Projets Info';
// auto conversion of datetime fields (require jquery-ui)
$_CONFIG['datetimefields']=array(	0=>'myfield',);
$_CONFIG['show_menu'] = True;

// Auth
$_CONFIG['noauth'] = False;

// Ldap (used if SSO enabled to verify group membership)
$_CONFIG['custom_ldap_class']='../vendor/ldapad.php';
$_CONFIG['ad_bind_user'] = ''; // Ldap user used to bind Ldap server
$_CONFIG['ad_bind_passwd']=''; // Ldap password used to bind Ldap server
$_CONFIG['ad_host'] = ''; // Ldap host
$_CONFIG['ad_domain'] = ''; // Ldap domain
$_CONFIG['ad_basedn'] = ''; // Ldap base DN

// Mail - SMTP
// Full path of swift_required.php file (and dependancies next to it obviously) - it can be a path to autoload (recommanded)
$_CONFIG['swiftmailer_path']='/var/www/vendor/autoload.php';
// Mail server details
$_CONFIG['domain']='';
$_CONFIG['smtp_server'] = '';
$_CONFIG['smtp_port'] = 25;
$_CONFIG['default_sender'] = 'youremail@yourdomain.tld';
// Send a blinded Carbon Copy of "boost" to another user
$_CONFIG['taskemail_bcc'] = 'anotheruser@yourdomain.tld';
// Send a blinded Carbon Copy of manager notification to another user
$_CONFIG['noticemanager_bcc'] = 'anotheruser@yourdomain.tld';
$_CONFIG['ldap_mail_as_sender'] = False;

// Template directory (have to be created first)
$_CONFIG['template']='00-TEMPLATE';
// Archive DIRECTORY
$_CONFIG['archive']='00-ARCHIVE';

// File upload
$_CONFIG['UploadMaxFileSize'] = 2097156; // Max filesize upload (octets)
$_CONFIG['UploadPath'] = 'uploads/'; // Uploads filepath, relative to installation_path, ending with /
$_CONFIG['UploadExtensions'] = array('gif','jpg','jpe','jpeg','png','pdf','odt','doc');

$_CONFIG['users']['admins'] = array('');
//$_CONFIG['users']['operators'] = array('');
$_CONFIG['users']['readonly'] = array('');
// Networks allowed to use application (array for a list of networks, False to allow all networks) - i.e : array('192.168.1', '192.168.0', '172.16.0');
$_CONFIG['allowed-networks'] = False;

// Base path will be your storage. End wil trailing slash "/".
$_CONFIG['basepath']='/opt/data/smartprojects';

// Default name for storage files (pfile=projet & tfile = task)
$_CONFIG['pfile']='project.ini';
$_CONFIG['tfile']='task.ini';
$_CONFIG['abfile']='adressbook.ini';

// Currency symbol (€, $, Ethereum, etc ;))
$_CONFIG['currency']='€';

// Define directly accessible buttons for projects - Buttons non listed here are accessible by a "more" button
// Possible values : 'setdpi', 'setmasterplan', 'setdone', 'refresh', 'datechanger', 'archive', 'toggletaskdone'
$_CONFIG['buttons']['project']=array('setdone','refresh','toggletaskdone');

// Define directly accessible buttons for tasks - Buttons non listed here are accessible by a "more" button
// Possible values : 'settaskdone', 'setcritical', 'taskemail', 'datechanger', 'noticemanager', 
$_CONFIG['buttons']['task']=array('settaskdone','setcritical');

// Default behavior for done task : to show (True) or to hide (False)
$_CONFIG['showdone'] = False;

// Local usernames and password - Set it to False into your config file to disable admin access
$_CONFIG['users']['admin']='YWRtaW4=';

// Adress book file


/**************/
/* CUSTOM TXT */
/**************/
// PATTERNS :
// [PROJECT] : Project name
// [TASK] : Task name
// [DEADLINE] : Task deadline
// [DEADLINESTRING] : Task Deadline in while letters (since X days)
// [USERFIRSTNAME] : Connected user first name (if known from ldap)
// [USERLASTNAME] : Connected user last name (if known from ldap)
// [USEREMAIL] :  : Connected user email (if known from ldap)

$_CONFIG['msg']['mailwithdeadline']='Hello,

You are contributing to the project "[PROJECT]".

Team needs you for the following task : "[TASK]".

We agreed that you come back to me with the task done on [DEADLINE] ([DEADLINESTRING]), would you contact me or give me news by email ?

Thanks.
';

$_CONFIG['msg']['mailwithoutdeadline']='Hello,

You are contributing to the project "[PROJECT]".

Team needs you for the following task : "[TASK]".

Would you contact me or give me news by email ?

Thanks.
';


$_CONFIG['msg']['noticemanagerwithoutdeadline']='Hello,

You are contributing to the task "[TASK]" of the project "[PROJECT]".

I would be gracefull if you can contact me as soon as you can give me news about it.

We keep in touch for any question.

Thanks.
';

$_CONFIG['msg']['noticemanagerwithdeadline']='Hello,

You are contributing to the task "[TASK]" of the project "[PROJECT]".

You are expected to give news about it at the lastest on [DEADLINE] ([DEADLINESTRING]).

We keep in touch for any question.

Thanks.
';

$_CONFIG['msg']['signature']='
--
[USERFIRSTNAME] [USERLASTNAME]
Project Manager
[USEREMAIL]
Your Company
Phone number';

?>
