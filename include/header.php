<!DOCTYPE html>
<html>
<head>
<title>SmartProjects v.1</title>
<link href="css/app.css" media="all" rel="stylesheet" type="text/css">
<link href="css/jquery-ui.min.css" media="all" rel="stylesheet" type="text/css">
<link href="css/jquery-ui-timepicker-addon.css" media="all" rel="stylesheet" type="text/css">
<link href="css/Chart.css" media="all" rel="stylesheet" type="text/css">

<!-- Chart.js -->
<script src="js/Chart.bundle.js"></script>
<!-- / Chart.js -->
</head>
