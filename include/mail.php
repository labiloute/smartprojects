<?php
	if (file_exists($_CONFIG['swiftmailer_path'])) require_once($_CONFIG['swiftmailer_path']);
?>
<?php
	//(file_exists('/var/www/vendor/swiftmailer/swiftmailer/lib/swift_required.php'))?require_once('/var/www/vendor/swiftmailer/swiftmailer/lib/swift_required.php'):NULL;//New version installed via composer
?>
<?php
class Mail {
	function __construct($CONFIG) {
		$this->CONFIG = $CONFIG;
		//echo $_SERVER['REMOTE_USER'];
	}
	//
	function SimplyEmail($dest,$subject,$message,$sender=False,$cc=False,$bcc=False)
		{
		//echo "CC : ".$cc;

		// Override sender if CONFIG is set
		if (!$sender) {
			$sender=$this->CONFIG['default_sender'];
			}
		if (!$this->mailer)
			{
			//echo 'Creation de la Classe';
			$this->transport = (new Swift_SmtpTransport($this->CONFIG['smtp_server'],$this->CONFIG['smtp_port']));
			$this->mailer = (new Swift_Mailer($this->transport));
			}

		// DEST : Eventually convert simple string to array
		if (! is_array($dest)) $dest = array($dest);

		// Compose
		$message = (new Swift_Message($subject))
	  			->setFrom(array($sender))
	  			->setTo($dest)
	 			->setBody($message)
				;
		
		if ($cc) {
			var_dump($cc); 
			if (! is_array($cc)) $cc = array($cc) ; 
			$message->setCc($cc); 
			}

		if ($bcc) {
			var_dump($bcc); 
			if (! is_array($bcc)) $bcc = array($bcc) ; 
			$message->setBcc($bcc); 
			}

		try {
			$result = $this->mailer->send($message);
		} 
		catch (Exception $e) {
	    		$txt_error='Problème d\'envoi de l\'email ! '.$e->getMessage();
			}

		//echo $txt_error;
		if ($result==1) {
			$this->last_error='Envoi d\'email OK' ;
			return True;
			}
		else {
			$this->last_error = $txt_error;
			return False;
			}
		}
	}
?>
