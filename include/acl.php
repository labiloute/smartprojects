<?php
class Acl {
	//
	function __construct($CONFIG) {
		$this->CONFIG = $CONFIG;
	}
	//
        function getClientIP_env() {
        /*
         if ($_GET['debug']) {
                echo "HTTP_CLIENT_IP : ".getenv('HTTP_CLIENT_IP').'<br />';
                echo "HTTP_X_FORWARDED_FOR : ".getenv('HTTP_X_FORWARDED_FOR').'<br />';
                echo "HTTP_X_FORWARDED : ".getenv('HTTP_X_FORWARDED').'<br />';
                echo "HTTP_FORWARDED_FOR : ".getenv('HTTP_FORWARDED_FOR').'<br />';
                echo "HTTP_FORWARDED : ".getenv('HTTP_FORWARDED').'<br />';
                echo "REMOTE_ADDR :".getenv('REMOTE_ADDR').'<br />';
                }
        */
        if (getenv('HTTP_CLIENT_IP'))
                $ip  = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR')) {
                $ip  = getenv('HTTP_X_FORWARDED_FOR');
                if (strpos($ip,',')) { $ip=explode(',',$ip)[0];}
                }
        else if(getenv('HTTP_X_FORWARDED'))
                $ip  = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
                $ip  = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
                $ip  = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
                $ip  = getenv('REMOTE_ADDR');
        else
                return False;
        return $ip;
        }


        //****************************/
        // Check if local IP match the config
        //****************************/
        function CheckAllowedNetworks() {
		// If security is disabled
		if (! $this->CONFIG['allowed-networks'] or count($this->CONFIG['allowed-networks'])==0) return True;

		// Get real client IP
                $remoteip = $this->getClientIP_env();

		//echo $remoteip;

		// Check that it belongs to allowed networks
                foreach ($this->CONFIG['allowed-networks'] as $network) {
                        if (strpos($remoteip, $network) !== false) {
                                return True;
                                }
                        }
                return False;
                }

        //****************************/
        // Local Auth
        //****************************/

	function LocalAuth($user,$pass) {

		// Read the JSON file
		//$config = file_get_contents($this->CONFIG['basepath'].'/config.json');

		//var_dump($json);
		// Decode the JSON file
		//$config_data = json_decode($json,true);

		// Display data
		//var_dump($config_data);
		//echo base64_encode('admin');
		if (base64_decode($this->CONFIG['users'][$user])===$pass) return True;
		else return False;
		}



	function GetProfile($groups) {

		// Loop thru Ldap groups if exists
		foreach ($groups as  $group) {
			//echo $group;
			if (in_array($group,$this->CONFIG['users']['admins'])) {
				$profile='RW';
				$_SESSION['profile']='RW';
				// The user has a valid group, we break
				break;
			}
			elseif (in_array($group,$this->CONFIG['users']['readonly'])) {
				$profile='RO';
				$_SESSION['profile']='RO';
			}
		}


		// If classic auth form has been posted, check it
		if (! $this->CONFIG['noauth'] && $_POST['user'] && $_POST['pass']) {
		$classicauth=$this->LocalAuth($_POST['user'],$_POST['pass']);

			if ($classicauth) {
				$profile='RW';
				$_SESSION['profile']='RW';
				}
			else {
				$profile = False;
				$_SESSION['profile']=False;
				}
			}

		// If at this stage, LDAP auth wasn't successfull, provide classic auth
		if (! $this->CONFIG['noauth'] && ! $_SESSION['profile']) {
			include_once('templates/login.php');
			}

		// Check that client Ip belongs to authorized ones
		$allowed_networks = $this->CheckAllowedNetworks();

		//var_dump($profile);
		//var_dump($allowed_networks);
		// Check that profile match authorized one
		if ((!$_SESSION['profile'] && ! $this->CONFIG['noauth']) or ! $allowed_networks){
			$profile=False;
			}
		// Maybe a read-only profile
		else if ($_SESSION['profile']=='RO'){
			echo '<div class="header">Read Only</div>';
			}
		//var_dump($profile);
		return $profile;
		}
	}
?>
