<?php
// NO BLANK SPACE IN THIS FILE PLEASE EXCEPT IN PHP CODE
session_start();
if (file_exists("config.default.php")) include_once("config.default.php");
if (file_exists("config.php")) include_once("config.php");
?>
<?php
        if ($_CONFIG['custom_ldap_class'] and file_exists('../'.$_CONFIG['custom_ldap_class'])) include_once('../'.$_CONFIG['custom_ldap_class']);
        else if ($_CONFIG['custom_ldap_class'] and file_exists($_CONFIG['custom_ldap_class'])) include_once($_CONFIG['custom_ldap_class']);
        else include_once("ldap.class.php");
?>
<?php include_once("../include/mail.php"); ?>
<?php $mail = new Mail($_CONFIG); ?>
<?php include_once("../include/acl.php"); ?>
<?php include_once("../include/adressbook.php"); ?>
<?php
$tmstp=time();
$ldap = new Ldap($_CONFIG);

if ($_CONFIG['ad_host'] and ! $_CONFIG['noauth']) {
	$ldap->connect();
	$username=$ldap->getUsername($_SERVER['REMOTE_USER']);
	$attr=$ldap->getAttributes($username,array('mail','givenname','sn'));
	($attr['mail'][0]!=NULL)?$useremail=$attr['mail'][0]:$useremail=NULL;
	//if ($useremail!=NULL){$s_mail = explode('@',$useremail);$useremail=$s_mail[0];}
	($attr['sn'][0]!=NULL && $attr['mail'][0]!=NULL)?$userlastname=$attr['sn'][0]:$userlastname=NULL;
	($attr['givenname'][0]!=NULL && $attr['mail'][0]!=NULL)?$userfirstname=$attr['givenname'][0]:$userfirstname=NULL;
	$useremail=$attr['mail'][0];
	// Mise en cache des groupes d'appartenance
	if (!$_SESSION['groups'])
		$groups = $ldap->getGroups($username);
		}

// Reset session value if requested
if ($_GET['logout'])unset($_SESSION);
// Create ACL checker object
$Acl = new Acl($_CONFIG);
// Init profile var (RW/RO)
$profile=$Acl->GetProfile($groups);

// Ajax commands are only available for RW profile
if ($_SESSION['profile']!='RW' && ! $_CONFIG['noauth']){
		echo "500. Forbidden !!";
		exit;
	}

$taskfile=$_CONFIG['tfile'];
$projectfile=$_CONFIG['pfile'];

include_once('../templates/project.php');
include_once('../templates/task.php');

$Task=new Task($_CONFIG);
$Project=new Project($_CONFIG,$Task);

include_once('../include/vars.php');

?>
<?php
function arr2ini(array $a, array $parent = array())
{
    $out = '';
    foreach ($a as $k => $v)
    {
        if (is_array($v))
        {
            //subsection case
            //merge all the sections into one array...
            $sec = array_merge((array) $parent, (array) $k);
            //add section information to the output
            $out .= '[' . join('.', $sec) . ']' . PHP_EOL;
            //recursively traverse deeper
            $out .= arr2ini($v, $sec);
        }
        else
        {
            //plain key->value case
            $out .= "$k=$v" . PHP_EOL;
        }
    }
    return $out;
}

function CreateDirectory($type,$path,$priority,$name,$setstartdate=False) {
	global $taskfile,$projectfile;
	$priority=intval($priority);
	($priority < 10)?$priority='0'.strval($priority):$priority=strval($priority);
	$directory=$priority.'-'.$name;
	if (is_dir($path.'/'.$directory)) {
		return "Élément déjà existant. Merci de modifier le nom ou l'ordre et réessayer.";
		}
	else
		{
		// null,True is here for windows
		$mkdir=mkdir($path.'/'.$directory,0777,True);
		//$chmod=chmod($path.'/'.$directory, );
		if ($mkdir) {
			if ($type=='project' && $setstartdate) setconfig('Time','start',date("d-m-Y"),$path.'/'.$directory.'/'.$projectfile);
			else if ($type=='task' && $setstartdate) setconfig('Time','start',date("d-m-Y"),$path.'/'.$directory.'/'.$taskfile);
			return "OK ! Refreshing...";
			}
		else
			return "[ERROR] Problème de creation du répertoire ".$path.'/'.$directory;
		}

	}

function CreateFromTemplate($project,$priority) {
	global $_CONFIG;
	$src=$_CONFIG['basepath'].$_CONFIG['template'];
	$priority=intval($priority);
	($priority < 10)?$priority='0'.strval($priority):$priority=strval($priority);
	$dest=$_CONFIG['basepath'].$priority.'-'.$project;
	//echo "Copie du modèle...";
	//shell_exec("cp -r $src $dest");
	$copy=custom_copy($src, $dest);
	return "Copy OK ! Refreshing...";
	}


function custom_copy($src, $dst) { 
   
	// open the source directory
    $dir = opendir($src); 
   
    // Make the destination directory if not exist
    @mkdir($dst); 
   
    // Loop through the files in source directory
    foreach (scandir($src) as $file) { 
   
        if (( $file != '.' ) && ( $file != '..' )) { 
            if ( is_dir($src . '/' . $file) ) 
            { 
   
                // Recursively calling custom copy function
                // for sub directory 
                custom_copy($src . '/' . $file, $dst . '/' . $file); 
   
            } 
            else { 
                copy($src . '/' . $file, $dst . '/' . $file); 
            } 
        } 
    } 
   
    closedir($dir);
    //return True;
}


?>
<?php



//*************************************/
// Refresh Project
if ($_POST['f'] and $_POST['action']=='refresh' and $_POST['pcount']) {
	$path=explode('/',$_POST['f']);
	array_pop($path);
	$path=implode('/',$path);
	//echo $_POST['pcount']." ".$_POST['f'];
	echo $Project->displayproject($_POST['pcount'],$path);
	exit;
	}
// SET/UNSET DPI
if ($_POST['f'] and $_POST['config']=='dpi') {
	setconfig('FHM','dpi',$_POST['value'],$_POST['f']);
}
// SET MANAGER
if ($_POST['f'] and $_POST['config']=='manager') {
	setconfig('Main','manager',$_POST['value'],$_POST['f']);
}
// SET TEAM
if ($_POST['f'] and $_POST['config']=='team') {
	setconfig('Main','team',$_POST['value'],$_POST['f']);
}
// SET COST
if ($_POST['f'] and $_POST['config']=='cost') {
	setconfig('Main','cost',$_POST['value'],$_POST['f']);
}
// SET/UNSET MASTERPLAN
if ($_POST['f'] and $_POST['config']=='masterplan') {
	setconfig('FHM','masterplan',$_POST['value'],$_POST['f']);
}

//*************************************/
// COMMON (task and/or project)

// COMMENT
if ($_POST['f'] and $_POST['config']=='comment') {
	setconfig('Main','comment',$_POST['value'],$_POST['f']);
}

// Change deadline
if ($_POST['f'] and $_POST['config']=='deadline') {
	setconfig('Main','deadline',$_POST['value'],$_POST['f']);
	// Set start value if not already set
	if ($_POST['start']=='') {
		echo "Note : Start date is empty, so we will assume an arbitrary start date based on deadline date.";
		$starttmstp=strtotime($_POST['start']);
		$deadlinetmstp=strtotime($_POST['value']);
		// If deadline of in the future, set start as now
		if ($deadlinetmstp>$tmstp) $start=date('d-m-Y');
		// Else, set start at the beginning of the same month as deadline
		else {
			$d=date('d',$deadlinetmstp);
			$m=date('m',$deadlinetmstp);
			$y=date('Y',$deadlinetmstp);
			if ($d=='01') {
				$fd='01';
				if ($m=='01') {
					$fm='12';
					$fy=intval($y)-1;
					}
				else {
					$fm=intval($m)-1;
					$fy=$y;
					}
				}
			else {$fd='01';$fm=$m;$fy=$y;}
			$start=date($fd.'-'.$fm.'-'.$fy,$deadlinetmstp);
			echo "Info : Start date changed to ".$start;
			}
		// Set start value
		setconfig('Time','start',$start,$_POST['f']);
		}
	else echo "Info : Start date unchanged";
}

// Change any date that allowed to be changed directly
$date_elements=array('last_reminder'=>'Time','start'=>'Time', 'done'=>'Main');
if ($_POST['f'] and array_key_exists($_POST['config'],$date_elements)) {
	// Case of "done" which can be 1 or 0
	if ($_POST['value']=='1') {
		$_POST['value']=date("d-m-Y");
		echo "Info : Tâche terminée tout de suite, le ".$_POST['value']." :)";
		}
	else if ($_POST['value']=='0')
		{
		$_POST['value']="";
		echo "Note : Tâche remise à l'état non terminée";
		}
	$element=$_POST['config'];
	$section=$date_elements[$element];
	setconfig($section,$_POST['config'],$_POST['value'],$_POST['f']);
	echo "Info : [".$section."] ".$_POST['config']." as been set to ".$_POST['value'];
}

//*************************************/
// TASKS
// SET/UNSET CRITICAL
if ($_POST['f'] and $_POST['config']=='critical') {
	setconfig('Main','critical',$_POST['value'],$_POST['f']);
	}


//*************************************/
// CHANGE PRIORITY (Task or Project)
if ($_POST['f'] and $_POST['config']=='priority') {

	// Get new priority and format it (0-9 on 2 char)
	$priority=$_POST['value'];
	if (strlen($priority)==1) $priority='0'.$priority;

	// Explode path a remove project.ini file
	$path=explode('/',$_POST['f']);
	array_pop($path);

	// Get project path if it is a task
	if ($_POST['type']=='task') {
		$project_path=$path;
		// Pop again the task value
		array_pop($project_path);
		// Reconstruct full project path
		$project_path=implode('/',$project_path);
		}

	$oldpath=implode('/',$path);

	//echo $oldpath;
	// Get Directory name
	$dir=end($path);
	//print_r($dir);
	// Explode, replace priority and reconstruct dir name
	$adir=explode('-',$dir);
	$adir[0]=$priority;
	$newdir=implode('-',$adir);
	array_pop($path);
	array_push($path,$newdir);
	$newpath=implode('/',$path);
	//echo "Oldpath : ".$oldpath." will become Newpath : ".$newpath."";
	// Write on disk
	rename($oldpath,$newpath);

	// Refresh whole projet if a task changed of priority
	if ($_POST['type']=='task' && intval($_POST['pid']) && $project_path) {
		$pcount=explode('-',$_POST['pid']);
		$pcount=intval($pcount[0]);
		// Give required info to generate project table
		echo $Project->displayproject($pcount,$project_path);
	}
}


//*************************************/
// RENAME (Task or Project)
//*************************************/
if ($_POST['f'] and $_POST['config']=='rename' and $_POST['value']) {
	$newname=$_POST['value'];
	$path=explode('/',$_POST['f']);
	// Pop the file name
	array_pop($path);
	// Get priority	
	$oldname=end($path);
	$priority=explode('-',$oldname);
	// Get priority given in form or fallback to extracted one
	if ($_POST['priority']) $priority=$_POST['priority'];
	else $priority=$priority[0];
	// Prepare destination
	$newpath=$path;
	array_pop($newpath);
	$newpath=implode('/',$newpath);
	$newpath=$newpath."/".$priority.'-'.$newname;
	// Get project name
	$elementname=end($path);
	// Reconstruct full project path
	$path=implode('/',$path);
	// Simply move it
	echo "Renaming from ".$path." to ".$newpath."";
	rename($path,$newpath);
}


//*************************************/
// FUNCTION TO SET A VALUE INTO A FILE
//*************************************/
function setconfig($category,$config,$value,$path) {
	//if ($category or !$config or !$value or !$path) return False;
	//echo "Modification du fichier ".$path.". On place [".$category."][".$config."] a la valeur ".$value."";
	$tconfig=parse_ini_file($path,True);
	// If there is no ini file, we set it with minimal values
	if (! $tconfig)
		$tconfig=array($category=>array($config=>$value));
	else
		$tconfig[$category][$config]=$value;
	// Convert array to ini format
	$a=arr2ini($tconfig);
	// Open file with write permissions
	$ffl=fopen($path,"w");
	// Write content
	fwrite($ffl,$a);
	// Close file
	fclose($ffl);
	//echo "Info : ".$config." has been set to ".$value." in file ".$path.".";
}

//*************************************/
// Mail Notification
//*************************************/
if ($_POST['taskemail'] && $_POST['manager'] && $_POST['t'] && $_POST['p']) {
	//echo "coucou!";
	if ($_POST['deadline'] && $_POST['deadline']!='' && $_POST['deadlinestring'])
		$content = $_CONFIG['msg']['mailwithdeadline'].$_CONFIG['msg']['signature'];
	else
		$content = $_CONFIG['msg']['mailwithoutdeadline'].$_CONFIG['msg']['signature'];
	
	$content=str_replace('[PROJECT]',$_POST['p'],$content);
	$content=str_replace('[TASK]',$_POST['t'],$content);
	$content=str_replace('[DEADLINE]',$_POST['deadline'],$content);
	$content=str_replace('[DEADLINESTRING]',$_POST['deadlinestring'],$content);
	$content=str_replace('[USERFIRSTNAME]',$userfirstname,$content);
	$content=str_replace('[USERLASTNAME]',$userlastname,$content);
	$content=str_replace('[USEREMAIL]',$useremail,$content);

	// We setup the last_reminder field to today, only if a task file is given
	if ($_POST['f']!="" and isset($_POST['f']))
		setconfig('Time','last_reminder',date("d-m-Y"),$_POST['f']);

	// Check sender
	if ($_CONFIG['ldap_mail_as_sender']) $sender=$useremail;
	else $sender = $_CONFIG['default_sender'];

	if ($_CONFIG['taskemail_bcc']) $bcc=$_CONFIG['taskemail_bcc'];
	else $bcc = False;

	// Check for multiple managers
	if ( strpos($_POST['manager'],',')) {$managers = explode(',',$_POST['manager']);}
	else $managers = array($_POST['manager']);

	$dest=array();
	foreach ($managers as $manager) {
		if (! strpos($manager,'@')) $manager=$manager.'@fondation-misericorde.fr';
		$dest[]=$manager;
		}

	$mail->SimplyEmail($dest,'Projet '.$_POST['p'].' - '.$_POST['t'],$content,$sender, False, $bcc);
	if ($mail->$txt_error) {
		echo $mail->$txt_error;
		}
	//echo $mail->last_error."<br />";
}

//*************************************/
// Added to project notification
//*************************************/
if ($_POST['noticemanager'] && $_POST['manager'] && $_POST['t'] && $_POST['p']) {
	//echo "coucou!";
	if ($_POST['deadline'] && $_POST['deadline']!='' && $_POST['deadlinestring'])
		$content = $_CONFIG['msg']['noticemanagerwithdeadline'].$_CONFIG['msg']['signature'];
	else
		$content = $_CONFIG['msg']['noticemanagerwithoutdeadline'].$_CONFIG['msg']['signature'];
	
	$content=str_replace('[PROJECT]',$_POST['p'],$content);
	$content=str_replace('[TASK]',$_POST['t'],$content);
	$content=str_replace('[DEADLINE]',$_POST['deadline'],$content);
	$content=str_replace('[DEADLINESTRING]',$_POST['deadlinestring'],$content);
	$content=str_replace('[USERFIRSTNAME]',$userfirstname,$content);
	$content=str_replace('[USERLASTNAME]',$userlastname,$content);
	$content=str_replace('[USEREMAIL]',$useremail,$content);

	// Check sender
	if ($_CONFIG['ldap_mail_as_sender']) $sender=$useremail;
	else $sender = $_CONFIG['default_sender'];

	if ($_CONFIG['noticemanager_bcc']) $bcc=$_CONFIG['noticemanager_bcc'];
	else $bcc = False;

	// Check for multiple managers
	if ( strpos($_POST['manager'],',')) {$managers = explode(',',$_POST['manager']);}
	else $managers = array($_POST['manager']);

	$dest=array();
	foreach ($managers as $manager) {
		if (! strpos($manager,'@')) $manager=$manager.'@fondation-misericorde.fr';
		$dest[]=$manager;
		}

	$mail->SimplyEmail($dest,'Projet '.$_POST['p'].' - '.$_POST['t'],$content,$sender, False, $bcc);
	if ($mail->$txt_error) {
		echo $mail->$txt_error;
		}
	} 


/* ****************************** */
// PROJECT/TASK CREATION
/* ****************************** */

if ($_POST['action']=='createproject' && $_POST['project'] && $_POST['priority']) {
	echo CreateDirectory('project',$_CONFIG['basepath'],$_POST['priority'],$_POST['project'],True);
	}

if ($_POST['action']=='createprojecttemplate' && $_POST['project'] && $_POST['priority']) {
	echo CreateFromTemplate($_POST['project'],$_POST['priority']);
	}

if ($_POST['action']=='createtask' && $_POST['pdirname'] && $_POST['task'] && $_POST['priority']) {
	// Create directory
	echo CreateDirectory('task',$_CONFIG['basepath'].'/'.$_POST['pdirname'],$_POST['priority'],$_POST['task'],True);
	}

/* ****************************** */
// PROJECT ARCHIVING
/* ****************************** */

if ($_POST['action']=='archive' && $_POST['f']) {
	// Get project path
	$path=explode('/',$_POST['f']);
	// Pop the file name
	array_pop($path);
	// Get project name
	$project=end($path);
	// Reconstruct full project path
	$project_path=implode('/',$path);
	// Construct destination
	$dest=$_CONFIG['basepath']."/".$_CONFIG['archive']."/".$project;
	// Simply move it
	echo "Moving from ".$project_path." to ".$dest."";
	rename($project_path,$dest);
	}


//*************************************/
// MANAGER/TEAM Auto-complete
//*************************************/
if ($_POST['action']=='adressbook') {
	$ABook=new AdressBook($_CONFIG);
	$ABook->GetFromFile();
	$list=$_SESSION['abook'];
	$matches = array();
	$searchword = $_POST['SearchTerm'];
	foreach($list as $k=>$v) {
		if(stripos($v,$searchword)!==False) {
			$matches[$k] = $v;
			}
		}

	//$list=array("Test","test","coucou","coincoin","meuhmeuh");
	if (!$_GET['callback']) $_GET['callback'] = '***calbacksalt***';
	echo $_GET['callback'].'('.json_encode($matches).')';
	}


?>
