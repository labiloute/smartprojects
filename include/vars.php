<?php

// Handle Archive
if ($_GET['archive']==="1") $_SESSION['archive']=True;
else if ($_GET['archive']==="0") $_SESSION['archive']=False;

if ($_SESSION['archive'] && is_dir($_CONFIG['basepath']."/".$_CONFIG['archive']) && $_CONFIG['archive']) {
	$basepath=$_CONFIG['basepath']."/".$_CONFIG['archive'];
	$archive_link='<a href="?archive=0" title="Retour à la liste des projets" class="circled">&#129512;</a>';
	}
else if (! $_SESSION['archive'] and $_CONFIG['archive'] and is_dir($_CONFIG['basepath']."/".$_CONFIG['archive']) ) {
	$basepath=$_CONFIG['basepath'];
	$archive_link='<a href="?archive=1" title="Afficher les archives" class="circled">&#129512;</a>';
	}
else {
	$archive_link='<a href="#" title="Please create archive folder ('.$_CONFIG['archive'].') to take benefit of archive functionnality." class="circled">&#129512;</a>';
	$basepath=$_CONFIG['basepath'];
	}

// Show task/project done
//if (!$_SESSION['showdone'])$_SESSION['showdone']=$_CONFIG['showdone'];

if ($_GET['showdone']==="1") {$_SESSION['showdone']=True;}
else if ($_GET['showdone']==="0") {$_SESSION['showdone']=False;}
else if (! isset($_SESSION['showdone'])) {$_SESSION['showdone']=$_CONFIG['showdone'];}
//var_dump($_SESSION['showdone']);
if ($_SESSION['showdone']) $showdone_link='<a href="?showdone=0" title="Masquer les tâches terminées" class="circled">&#9745;</a>';
else if ( ! $_SESSION['showdone']) $showdone_link='<a href="?showdone=1" title="Afficher les tâches terminées" class="circled">&#9989;</a>';
else if ( $_CONFIG['showdone'] ) $showdone_link='<a href="?showdone=0" title="Masquer les tâches terminées" class="circled">&#9745;</a>';
else if ( ! $_CONFIG['showdone'] ) $showdone_link='<a href="?showdone=1" title="Afficher les tâches terminées" class="circled">&#9989;</a>';

?>
